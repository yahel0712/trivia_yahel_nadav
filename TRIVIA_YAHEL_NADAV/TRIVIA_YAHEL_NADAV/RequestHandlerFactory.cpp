#include "RequestHandlerFactory.h"

RequestHandlerFactory* RequestHandlerFactory::instanceLoginRH = nullptr;


RequestHandlerFactory::RequestHandlerFactory(IDatabase* db):
	m_StatisticsManager(StatisticsManager(db)), m_database(db)
{
	this->m_loginManager = LoginManager::getinstanceLoginManager(this->m_database);
	this->m_roomManager = RoomManager();
}

RequestHandlerFactory* RequestHandlerFactory::getInstanceLoginRH(IDatabase* db)
{
	if (instanceLoginRH == nullptr)
	{
		instanceLoginRH = new RequestHandlerFactory(db);
	}
	return instanceLoginRH;
}

MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler()
{
	return new MenuRequestHandler(this->m_loginManager->getUsers(),
		this->getRoomManager(),
		this->getStatisticsManager(), *this);
}

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
	return new LoginRequestHandler(*this->m_loginManager, *this);
}

RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(
	LoggedUser admin, Room room)
{
	return new RoomAdminRequestHandler(room, admin, this->m_roomManager, *this);
}

RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(
	LoggedUser member, Room room)
{
	return new RoomMemberRequestHandler(room, member, this->m_roomManager, *this);
}


LoginManager& RequestHandlerFactory::getLoginManager()
{
	return *this->m_loginManager;
}

RoomManager& RequestHandlerFactory::getRoomManager()
{
	return this->m_roomManager;
}

StatisticsManager& RequestHandlerFactory::getStatisticsManager()
{
	return this->m_StatisticsManager;
}
