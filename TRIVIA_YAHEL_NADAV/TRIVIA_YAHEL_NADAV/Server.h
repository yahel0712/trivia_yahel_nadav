#pragma once
#include "Communicator.h"
#include "RequestHandlerFactory.h"

class Server
{
public:
	~Server() = default;

	static Server* getInstanceServer();
	void run();

private:
	Server();

	static Server* instanceServer;
	Communicator m_communicator;
	IDatabase* m_database;
	RequestHandlerFactory m_handlerFactory;
};

