#include "MenuRequestHandler.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "Exceptions.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"

#define CreateRoomCode 5
#define GetRoomsCode 6
#define GetPlayersInRoomCode 7
#define JoinRoomCode 4
#define GetHighScoreCode 8
#define LogoutCode 3
#define getRoomCode 9




MenuRequestHandler::MenuRequestHandler(std::set<LoggedUser>& users, RoomManager& roomManager,
    StatisticsManager& statisticsManager, RequestHandlerFactory& handlerFactory) :
    m_roomManager(roomManager), m_statisticsManager(statisticsManager),
    m_handlerFactory(handlerFactory), m_users(users)
{
}



RequestResult MenuRequestHandler::getRooms(RequestInfo reqInfo)
{
    getRoomsResponse gsr;
    RequestResult rs;
    string roomsString;

    try
    {
        vector<Room> rooms = this->m_roomManager.getRooms();
        for (auto room : rooms)
        {

            roomsString += room.getDataRoom().name + ',';
            
        }
        roomsString[roomsString.size()] = NULL;

        gsr.rooms = roomsString;
        rs.buffer = JsonResponsePacketSerializer::serializeResponse
        (gsr);

        //return the current menuRequestHandler
        rs.newHandler = this;
    }
    catch (const std::exception& ex)
    {
        ErrorResponse er = ErrorResponse();
        er.message = ex.what();

        rs.newHandler = this;
        rs.buffer = JsonResponsePacketSerializer::serializeResponse
        (er);
        return rs;
    }


    return rs;
}

RequestResult MenuRequestHandler::getRoom(RequestInfo reqInfo)
{
    RequestResult rs;
    try
    {
        getRoomResponse grr;
        getRoomRequest getRoomReq = JsonRequestPacketDeserializer::
            deserializeGetRoomRequest(reqInfo.buffer);

        RoomData roomd = this->m_roomManager.getRoom(getRoomReq.roomName).
            getDataRoom();
        grr.name = roomd.name;
        grr.admin = roomd.admin;

        for (auto user : this->m_roomManager.getRoom(getRoomReq.roomName).
            getUsers())
        {
            grr.users += user.getUsername() + ',';
        }
        grr.users[grr.users.size()] = NULL;

        rs.buffer = JsonResponsePacketSerializer::serializeResponse(grr);
        rs.newHandler = this;
    }
    catch (const std::exception& ex)
    {
        ErrorResponse er = ErrorResponse();
        er.message = ex.what();

        rs.newHandler = this;
        rs.buffer = JsonResponsePacketSerializer::serializeResponse
        (er);
        return rs;
    }

    return rs;
}

RequestResult MenuRequestHandler::getPlayersInRoom(RequestInfo reqInfo)
{
    RequestResult rs;

    try
    {
        getPlayersInRoomResponse gpr;
        GetPlayersInRoomRequest playersReq = JsonRequestPacketDeserializer::
            deserializeGetPlayersInRoomRequest(reqInfo.buffer);

        string users;
        for (auto user : this->m_roomManager.getRoom(playersReq.roomName).getUsers())
        {
            users += user.getUsername() + ',';
        }
        users[users.size()] = NULL;

        gpr.users = users;
        rs.buffer = JsonResponsePacketSerializer::serializeResponse
        (gpr);

        // return the current menuRequestHandler
        rs.newHandler = this;
    }
    catch (const std::exception& ex)
    {
        ErrorResponse er = ErrorResponse();
        er.message = ex.what();

        rs.newHandler = this;
        rs.buffer = JsonResponsePacketSerializer::serializeResponse
        (er);
        return rs;
    }
    return rs;
}

bool MenuRequestHandler::isRequestRelevant(RequestInfo reqInfo)
{
    if (reqInfo.id == CreateRoomCode || reqInfo.id == GetHighScoreCode ||
        reqInfo.id == GetRoomsCode || reqInfo.id == JoinRoomCode ||
        reqInfo.id == LogoutCode || reqInfo.id == GetPlayersInRoomCode ||
        reqInfo.id == getRoomCode)
    {
        return true;
    }
    return false;
}

RequestResult MenuRequestHandler::handleRequest(RequestInfo reqInfo)
{
    if (reqInfo.id == CreateRoomCode)
    {
        return createRoom(reqInfo);
    }
    else if (reqInfo.id == JoinRoomCode)
    {
        return joinRoom(reqInfo);
    }
    else if (reqInfo.id == GetRoomsCode)
    {
        return getRooms(reqInfo);
    }
    else if (reqInfo.id == getRoomCode)
    {
        return getRoom(reqInfo);
    }
    else if (reqInfo.id == GetPlayersInRoomCode)
    {
        return getPlayersInRoom(reqInfo);
    }
    else if (reqInfo.id == GetHighScoreCode)
    {
        //return getHighScore(reqInfo);
    }
    else if (reqInfo.id == LogoutCode)
    {
       return signout(reqInfo);
    }
}

RequestResult MenuRequestHandler::signout(RequestInfo reqInfo)
{
    RequestResult requestRes;
    try
    {
        LogoutRequest signoutReq = JsonRequestPacketDeserializer::deserializeLogoutRequest(
            reqInfo.buffer);
        this->m_roomManager.removeUserFromAllRooms(signoutReq.userName);

        // remove the user form the online users
        this->m_users.erase(LoggedUser(signoutReq.userName));
          
    }
    catch (const std::exception& ex)
    {
        ErrorResponse er = ErrorResponse();
        er.message = ex.what();

        requestRes.newHandler = this;
        requestRes.buffer = JsonResponsePacketSerializer::serializeResponse
        (er);
        return requestRes;
    }

    requestRes.newHandler = this;
    requestRes.buffer = JsonResponsePacketSerializer::serializeResponse
    (LogoutResponse());

    return requestRes;
}

RequestResult MenuRequestHandler::joinRoom(RequestInfo reqInfo)
{
    RequestResult requestRes;

    try
    {
        JoinRoomRequest joinRoomReq = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(
            reqInfo.buffer);

        Room roomToJoin = this->m_roomManager.getRoom(joinRoomReq.roomName);
        
        if (roomToJoin.getDataRoom().maxPlayers == 
            roomToJoin.getAllUsers().size())
        {
            throw MaxPlayersInRoomException();
        }

        this->m_roomManager.addUserToRoom(joinRoomReq.username,
            joinRoomReq.roomName);

        JoinRoomResponse jrr;
        jrr.status = 1;
        requestRes.newHandler = this->m_handlerFactory.createRoomMemberRequestHandler(
            LoggedUser(joinRoomReq.username), this->m_roomManager.getRoom(joinRoomReq.roomName));
        requestRes.buffer = JsonResponsePacketSerializer::serializeResponse(jrr);

        return requestRes;
    }
    catch (const std::exception& ex)
    {
        ErrorResponse er = ErrorResponse();
        er.message = ex.what();

        requestRes.newHandler = this;
        requestRes.buffer = JsonResponsePacketSerializer::serializeResponse
        (er);
        return requestRes;
    }


}

RequestResult MenuRequestHandler::createRoom(RequestInfo reqInfo)
{
    RequestResult requestRes;

    try
    {
        CreateRoomRequest createRoomReq = JsonRequestPacketDeserializer::
            deserializeCreateRoomRequest(
                reqInfo.buffer);

        RoomData newRoomData;
        newRoomData.admin = createRoomReq.admin;
        newRoomData.name = createRoomReq.roomName;
        newRoomData.maxPlayers = createRoomReq.maxUsers;
        newRoomData.numOfQuestionInGame = createRoomReq.questionCount;
        newRoomData.timePerQuestion = createRoomReq.answerTimeout;
        newRoomData.hasGameBegan = false;

        LoggedUser admin(createRoomReq.admin);
 
        // create the room
        this->m_users.insert(admin);
        this->m_roomManager.createRoom(admin, newRoomData);
        bool roomExist = false;

        requestRes.newHandler = this->m_handlerFactory
            .createRoomAdminRequestHandler(
                admin, this->m_roomManager.getRoom(createRoomReq.roomName));


        CreateRoomResponse crr;
        crr.status = 1;
        requestRes.buffer = JsonResponsePacketSerializer::serializeResponse
        (crr);

        return requestRes;
    }
    catch (const std::exception& ex)
    {
        ErrorResponse er = ErrorResponse();
        er.message = ex.what();

        requestRes.newHandler = this;
        requestRes.buffer = JsonResponsePacketSerializer::serializeResponse
        (er);
        return requestRes;
    }
}
