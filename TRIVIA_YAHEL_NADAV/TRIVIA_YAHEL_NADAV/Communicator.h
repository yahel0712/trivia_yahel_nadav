#pragma once
#include <map>
#include <WinSock2.h>
#include <Windows.h>
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"


using std::map;

class Communicator
{
public:
	~Communicator();

	static Communicator* getInstanceComm(RequestHandlerFactory& hf);
	void operator=(const Communicator& other);

	void startHandlerRequests();

	void accept();
private:
	Communicator(RequestHandlerFactory& hf);

	void bindAndListen();
	void handleNewClient(SOCKET client_socket);

	static Communicator* instanceCommunicator;

	SOCKET m_serverSocket;
	map <SOCKET, IRequestHandler*> m_clients;
	RequestHandlerFactory& m_handlerFactory;
};

