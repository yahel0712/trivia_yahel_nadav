#include "Server.h"
#include "Communicator.h"
#include <thread>
#include <iostream>
#include <WinSock2.h>
#include <Windows.h>
#include "SqliteDateBase.h"


using std::thread;

Server* Server::instanceServer = nullptr;


Server::Server() : m_database(SqliteDataBase::getInstanceSqliteDB()),
m_handlerFactory(*RequestHandlerFactory::getInstanceLoginRH(m_database)),
m_communicator(*Communicator::getInstanceComm(m_handlerFactory))
{
}

Server* Server::getInstanceServer()
{
	if (instanceServer == nullptr)
	{
		instanceServer = new Server();
	}
	return instanceServer;
}

void Server::run()
{
	std::string adminInput = "";

	// open new thread.
	thread t(&Communicator::startHandlerRequests, this->m_communicator);

	t.detach();
	
	while (true)
	{
		std::cin >> adminInput;
		if (adminInput == "EXIT")
		{
			break;
		}
	}
}
