#include "Room.h"

Room::Room(RoomData data, LoggedUser logUser)
    : m_metadata(data)
{
    addUser(logUser);
}

// fucntion add user, push to the vector of the users.
void Room::addUser(LoggedUser logUser)
{
	this->m_users.push_back(logUser);
    
    if (this->m_users.size() > 1)
    {
        this->m_metadata.isActive = true;
    }
}

void Room::removeUser(LoggedUser logUser)
{
    for (auto i = this->m_users.begin(); i != this->m_users.end(); i++)
    {
        // search for the user to erase.
        if (i->getUsername() == logUser.getUsername())
        {
            this->m_users.erase(i);
            break;
        }
    }

    if (this->m_users.size() <= 1)
    {
        this->m_metadata.isActive = false;
    }
}

vector<string> Room::getAllUsers()
{
    vector<string> allUsersName;
    for (auto i : this->m_users)
    {
        allUsersName.push_back(i.getUsername());
    }
    return allUsersName;
}

RoomData Room::getDataRoom()
{
    return this->m_metadata;
}

string Room::getName()
{
    return this->m_metadata.name;
}

vector<LoggedUser> Room::getUsers()
{
    return this->m_users;
}

void Room::setActiveRoom(unsigned int active)
{
    this->m_metadata.isActive = active;
}

void Room::setHasGameBegan(bool ifGameBegan)
{
    this->m_metadata.hasGameBegan = ifGameBegan;
}
