#include "LoginRequestsHandler.h"
#include "Helper.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "MenuRequestHandler.h"
#include <exception>


#define LOG_IN_TYPE_CODE 1
#define SIGN_UP_TYPE_CODE 2


LoginRequestHandler::LoginRequestHandler(
    LoginManager& loginManager, RequestHandlerFactory& ReqHandlerFactory)
    : m_handlerFactory(ReqHandlerFactory), m_loginManager(loginManager)
{
}


bool LoginRequestHandler::isRequestRelevant(RequestInfo reqInfo)
{
    return (reqInfo.id == LOG_IN_TYPE_CODE || reqInfo.id == SIGN_UP_TYPE_CODE);
}


RequestResult LoginRequestHandler::handleRequest(RequestInfo reqInfo)
{
    if (reqInfo.id == LOG_IN_TYPE_CODE)
    {
        return login(reqInfo);
    }
    else if (reqInfo.id == SIGN_UP_TYPE_CODE)
    {
        return signup(reqInfo);
    }
}


RequestResult LoginRequestHandler::login(RequestInfo reqInfo)
{
    LoginRequest loginReq;
    RequestResult requestRes;
    loginReq = JsonRequestPacketDeserializer::deserializeLoginRequest(reqInfo.buffer);

    // try to login and catch the errors if exist
    try
    {
        this->m_loginManager.login(loginReq.username, loginReq.password);
    }
    catch (const std::exception& ex)
    {
        ErrorResponse er = ErrorResponse();
        er.message = ex.what();

        requestRes.newHandler = this;
        requestRes.buffer = JsonResponsePacketSerializer::serializeResponse
        (er);
        return requestRes;
    }
    

    requestRes.newHandler = new MenuRequestHandler(
        this->m_loginManager.getUsers(),
        this->m_handlerFactory.getRoomManager(),
        this->m_handlerFactory.getStatisticsManager(),
        this->m_handlerFactory);

    requestRes.buffer = JsonResponsePacketSerializer::serializeResponse
    (LoginResponse());

    return requestRes;
}


RequestResult LoginRequestHandler::signup(RequestInfo reqInfo)
{
    SignupRequest signupReq;
    RequestResult requestRes;
    signupReq = JsonRequestPacketDeserializer::deserializeSignupRequest(
        reqInfo.buffer);


    // try to login and catch the errors if exist
    try
    {
        this->m_loginManager.signup(signupReq.username,
            signupReq.password, signupReq.email, signupReq.address,
            signupReq.phone, signupReq.birthday);
    }
    catch (const std::exception& ex)
    {
        ErrorResponse er = ErrorResponse();
        er.message = ex.what();

        requestRes.newHandler = this;
        requestRes.buffer = JsonResponsePacketSerializer::serializeResponse
        (er);
        return requestRes;
    }


    requestRes.newHandler = new MenuRequestHandler(
        this->m_loginManager.getUsers(),
        this->m_handlerFactory.getRoomManager(),
        this->m_handlerFactory.getStatisticsManager(),
        this->m_handlerFactory);
    requestRes.buffer = JsonResponsePacketSerializer::serializeResponse
    (SignupResponse());

    return requestRes;
}
