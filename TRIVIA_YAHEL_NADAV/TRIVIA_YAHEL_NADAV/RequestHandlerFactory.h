#pragma once
#include "LoginManager.h"
#include "LoginRequestsHandler.h"
#include "RoomManager.h"
#include "MenuRequestHandler.h"
#include "StatisticsManager.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"

class LoginRequestHandler;
class StatisticsManager;
class MenuRequestHandler;
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;

class RequestHandlerFactory
{
public:
	~RequestHandlerFactory() = default;

	static RequestHandlerFactory* getInstanceLoginRH(IDatabase* db);

	MenuRequestHandler* createMenuRequestHandler();
	LoginRequestHandler* createLoginRequestHandler();
	RoomAdminRequestHandler* createRoomAdminRequestHandler(LoggedUser admin, Room room);
	RoomMemberRequestHandler* createRoomMemberRequestHandler(LoggedUser member, Room room);

	LoginManager& getLoginManager();
	RoomManager& getRoomManager();
	StatisticsManager& getStatisticsManager();

private:
	RequestHandlerFactory(IDatabase* db);
	RequestHandlerFactory() = default;

	static RequestHandlerFactory* instanceLoginRH;

	RoomManager m_roomManager;
	StatisticsManager m_StatisticsManager;

	LoginManager* m_loginManager;
	IDatabase* m_database;
};

