#pragma once
#include <string>

using std::string;

struct User
{
	string username;
	string password;
	string mail;
	string address;
	string phone;
	string birthday;
};