#include "JsonResponsePacketSerializer.h"
#include "Helper.h"
#include "json.hpp"

#define BYTE_SIZE 8
#define FOUR_BYTES 32
#define INT_BYTES 4

using std::cout;
using std::endl;


using nlohmann::json;


vector<unsigned char> JsonResponsePacketSerializer::SerializerResponse(
	string msgContent, char msgCode)
{
	vector<unsigned char> buffer;

	json Json;
	int BytesNum = 0;


	// add the code of the msg to the buffer
	Helper::insertStringIntoCharVector(buffer, Helper::charToBytes(msgCode));


	// the json of the response built by the type of the response
	LoginResponse lr;
	SignupResponse sr;
	LogoutResponse lor;
	JoinRoomResponse jrr;
	CreateRoomResponse crr;
	CloseRoomResponse closeR;
	StartGameResponse srr;
	LeaveRoomResponse lrr;
	if (msgCode != lr.msgCode || msgCode != sr.msgCode ||
		msgCode != lor.msgCode || msgCode != jrr.msgCode ||
		msgCode != crr.msgCode || msgCode != closeR.msgCode
		|| msgCode != srr.msgCode || msgCode != lrr.msgCode)
	{
		Json["message"] = msgContent;
	}
	else
	{
		Json["status"] = msgContent;
	}


	// convert the json into string
	string jsonRaw = Json.dump();

	// convert every char to binary
	string msgBytes;
	for (auto c : jsonRaw)
	{
		msgBytes += Helper::charToBytes(c);
		BytesNum++;
	}

	// convert the num of msg bytes from int to string
	string msgLength = std::to_string(BytesNum);
	msgLength = string(INT_BYTES - msgLength.size(), '0') + msgLength;


	// convert the string of the msg length bytes to binary
	string bytesNumBinary;
	for (auto letter : msgLength)
	{
		bytesNumBinary += Helper::charToBytes(letter);
	}


	// add the msg length to the buffer
	Helper::insertStringIntoCharVector(buffer, bytesNumBinary);


	// add the msg to the buffer
	Helper::insertStringIntoCharVector(buffer, msgBytes);


	return buffer;
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(ErrorResponse er)
{
	return SerializerResponse(er.message, er.msgCode);
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LoginResponse lr)
{
	return SerializerResponse(std::to_string(lr.status), lr.msgCode);
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(SignupResponse sr)
{
	return SerializerResponse(std::to_string(sr.status), sr.msgCode);
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LogoutResponse lr)
{
	return SerializerResponse(std::to_string(lr.status), lr.msgCode);
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(getRoomResponse gtr)
{
	vector<unsigned char> buffer;

	json Json;
	int BytesNum = 0;


	// add the code of the msg to the buffer
	Helper::insertStringIntoCharVector(buffer, Helper::charToBytes(gtr.msgCode));

	Json["name"] = gtr.name;
	Json["admin"] = gtr.admin;
	Json["users"] = gtr.users;


	// convert the json into string
	string jsonRaw = Json.dump();

	// convert every char to binary
	string msgBytes;
	for (auto c : jsonRaw)
	{
		msgBytes += Helper::charToBytes(c);
		BytesNum++;
	}

	// convert the num of msg bytes from int to string
	string msgLength = std::to_string(BytesNum);
	msgLength = string(INT_BYTES - msgLength.size(), '0') + msgLength;


	// convert the string of the msg length bytes to binary
	string bytesNumBinary;
	for (auto letter : msgLength)
	{
		bytesNumBinary += Helper::charToBytes(letter);
	}


	// add the msg length to the buffer
	Helper::insertStringIntoCharVector(buffer, bytesNumBinary);


	// add the msg to the buffer
	Helper::insertStringIntoCharVector(buffer, msgBytes);


	return buffer;
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(getPlayersInRoomResponse gpr)
{
	vector<unsigned char> buffer;

	json Json;
	int BytesNum = 0;


	// add the code of the msg to the buffer
	Helper::insertStringIntoCharVector(buffer, Helper::charToBytes(gpr.msgCode));

	Json["players"] = gpr.users;


	// convert the json into string
	string jsonRaw = Json.dump();

	// convert every char to binary
	string msgBytes;
	for (auto c : jsonRaw)
	{
		msgBytes += Helper::charToBytes(c);
		BytesNum++;
	}

	// convert the num of msg bytes from int to string
	string msgLength = std::to_string(BytesNum);
	msgLength = string(INT_BYTES - msgLength.size(), '0') + msgLength;


	// convert the string of the msg length bytes to binary
	string bytesNumBinary;
	for (auto letter : msgLength)
	{
		bytesNumBinary += Helper::charToBytes(letter);
	}


	// add the msg length to the buffer
	Helper::insertStringIntoCharVector(buffer, bytesNumBinary);


	// add the msg to the buffer
	Helper::insertStringIntoCharVector(buffer, msgBytes);


	return buffer;
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse jrr)
{
	return SerializerResponse(std::to_string(jrr.status), jrr.msgCode);
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse crr)
{
	return SerializerResponse(std::to_string(crr.status), crr.msgCode);
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(HighScoreResponse hsr)
{
	return SerializerResponse(hsr.message, hsr.msgCode);
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(getRoomsResponse er)
{
	vector<unsigned char> buffer;

	json Json;
	int BytesNum = 0;


	// add the code of the msg to the buffer
	Helper::insertStringIntoCharVector(buffer, Helper::charToBytes(er.msgCode));

	Json["Rooms"] = er.rooms;


	// convert the json into string
	string jsonRaw = Json.dump();

	// convert every char to binary
	string msgBytes;
	for (auto c : jsonRaw)
	{
		msgBytes += Helper::charToBytes(c);
		BytesNum++;
	}

	// convert the num of msg bytes from int to string
	string msgLength = std::to_string(BytesNum);
	msgLength = string(INT_BYTES - msgLength.size(), '0') + msgLength;


	// convert the string of the msg length bytes to binary
	string bytesNumBinary;
	for (auto letter : msgLength)
	{
		bytesNumBinary += Helper::charToBytes(letter);
	}


	// add the msg length to the buffer
	Helper::insertStringIntoCharVector(buffer, bytesNumBinary);


	// add the msg to the buffer
	Helper::insertStringIntoCharVector(buffer, msgBytes);


	return buffer;
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse crr)
{
	return SerializerResponse(std::to_string(crr.status), crr.msgCode);
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(StartGameResponse sgr)
{
	return SerializerResponse(std::to_string(sgr.status), sgr.msgCode);
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse lrr)
{
	return SerializerResponse(std::to_string(lrr.status), lrr.msgCode);
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse grs)
{
	vector<unsigned char> buffer;

	json Json;
	int BytesNum = 0;


	// add the code of the msg to the buffer
	Helper::insertStringIntoCharVector(buffer, Helper::charToBytes(grs.msgCode));

	Json["status"] = grs.status;
	Json["hasGameBegan"] = grs.hasGameBegan;
	Json["players"] = grs.players;
	Json["admin"] = grs.admin;


	// convert the json into string
	string jsonRaw = Json.dump();

	// convert every char to binary
	string msgBytes;
	for (auto c : jsonRaw)
	{
		msgBytes += Helper::charToBytes(c);
		BytesNum++;
	}

	// convert the num of msg bytes from int to string
	string msgLength = std::to_string(BytesNum);
	msgLength = string(INT_BYTES - msgLength.size(), '0') + msgLength;


	// convert the string of the msg length bytes to binary
	string bytesNumBinary;
	for (auto letter : msgLength)
	{
		bytesNumBinary += Helper::charToBytes(letter);
	}


	// add the msg length to the buffer
	Helper::insertStringIntoCharVector(buffer, bytesNumBinary);


	// add the msg to the buffer
	Helper::insertStringIntoCharVector(buffer, msgBytes);


	return buffer;
}