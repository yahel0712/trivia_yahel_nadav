#include "Helper.h"
#include "Exceptions.h"
#include <bitset>
#include <regex>
#include "Structs.h"
#include <list>

#define BYTE_SIZE 8
#define PASSWORD_LENGTH 8
#define MIN_SPECIAL_CHAR 32
#define MAX_SPECIAL_CHAR 42
#define MIN_DIGIT 48
#define MAX_DIGIT 57
#define MIN_BIG_LETTER 65
#define MAX_BIG_LETTER 90
#define MIN_SMALL_LETTER 97
#define MAX_SMALL_LETTER 122
#define VALIDATE_COUNT 4

using std::regex;
using std::list;

string Helper::allBytesToString(vector<unsigned char> buffer)
{
	string helper = "", full_msg = "";

	// convert the buffer data to string (from bytes to chars).
	for (int i = 0; i < (buffer.size()); i++)
	{
		helper += buffer[i];

		if ((i + 1) % BYTE_SIZE == 0)
		{
			full_msg += Helper::bytesToChar(helper);
			helper = "";
		}
	}
	return full_msg;
}


/*
* the function convert bytes string to char
*/
char Helper::bytesToChar(string bytesToConvert)
{
	return std::bitset<8>(bytesToConvert).to_ulong();
}


/*
* function return number of bytes ofter convert to string.
*/
string Helper::numberOfBytesToString(vector<unsigned char> buffer, int numberOfBytes)
{
	string afterConvert = "", helper = "";
	// convert the buffer data to string (from bytes to chars).

	for (int i = 0; i < (numberOfBytes * BYTE_SIZE); i++)
	{
		helper += buffer[i];
	
		if ((i + 1) % BYTE_SIZE == 0)
		{
			afterConvert += Helper::bytesToChar(helper);
			helper = "";
		}
	}

	return afterConvert;
}


/*
* the function get char and return the char as int array  of bytes
*/
string Helper::charToBytes(char charToConvert)
{
    return std::bitset<BYTE_SIZE>(charToConvert).to_string();
}

/*
* the function get reference to vector of chars and insert the string
into the vector
*/
void Helper::insertStringIntoCharVector(vector<unsigned char>& charV,
    string stringToInsert)
{
    for (auto chatToInsert : stringToInsert)
    {
        charV.push_back(chatToInsert);
    }
}

/*
* the function get string of bytes and convert it to int
*/
int Helper::bytesToInt(string bytesToConvert)
{
	int n = 0, helper = 0, decimalNumber = 0;
	string byteHelper = "";

	for (int i = 0; i < bytesToConvert.size(); i++)
	{
		byteHelper += bytesToConvert[i];

		if ((i + 1) % BYTE_SIZE == 0)
		{
			decimalNumber *= 10;

			helper = ((int)Helper::bytesToChar(string(byteHelper)) - '0');
			byteHelper = "";

			decimalNumber += helper;
		}
	}

	return decimalNumber;
}


string Helper::convertVectorCharsToString(vector<unsigned char> buffer)
{
	string toConvert = "";

	for (auto i : buffer)
	{
		toConvert += i;
	}
	return toConvert;
}

Statistics Helper::getStatisticsAverages(list<Statistics> statList)
{
	Statistics avg;
	for (auto stat : statList)
	{
		avg.avgTime += stat.avgTime;
		avg.correctAnswers += stat.correctAnswers;
		avg.points += stat.points;
		avg.totalAnswers += stat.totalAnswers;
		avg.username = stat.username;
	}
	avg.avgTime /= statList.size();

	return avg;
}

void Helper::validPassword(string password)
{
	int specialCounter = 0, digitCounter = 0, smallLetterCounter = 0,
		capitalLetterCounter = 0;
	if (password.size() == PASSWORD_LENGTH)
	{
		regex rx("[!@#$%^&*]*[a-z0-9A-Z]*[!@#$%^&*]*[a-z0-9A-Z]*");
		for (auto letter : password)
		{
			int charAsciiValue = int(letter);
			
			// check if the char is special char
			if (charAsciiValue >= MIN_SPECIAL_CHAR &&
				charAsciiValue <= MAX_SPECIAL_CHAR)
			{
				specialCounter++;
			}

			// check if the char is a digit
			if (charAsciiValue >= MIN_DIGIT &&
				charAsciiValue <= MAX_DIGIT)
			{
				digitCounter++;
			}

			// check if the char is a small letter
			if (charAsciiValue >= MIN_SMALL_LETTER &&
				charAsciiValue <= MAX_SMALL_LETTER)
			{
				smallLetterCounter++;
			}

			// check if the char is a capital letter
			if (charAsciiValue >= MIN_BIG_LETTER &&
				charAsciiValue <= MAX_BIG_LETTER)
			{
				capitalLetterCounter++;
			}
		}

		if (!(specialCounter && digitCounter && smallLetterCounter &&
			capitalLetterCounter))
		{
			throw passwordException();
		}

	}
	else
	{
		throw passwordException();
	}
}

void Helper::validMail(string mail)
{
	regex rx("[a-zA-Z0-9]+@[a-zA-Z]+[.][a-zA-Z]+");
	if (!std::regex_match(mail, rx))
	{
		throw emailException();
	}
}

void Helper::validAddress(string address)
{
	regex rx("[a-zA-Z]*, [0-9]*, [a-zA-Z]*");
	if (!std::regex_match(address, rx))
	{
		throw addressException();
	}
}

void Helper::validPhone(string phone)
{
	regex rx("0[0-9]{8,9}");
	if (!std::regex_match(phone, rx))
	{
		throw phoneException();
	}
}

void Helper::validDate(string date)
{
	regex rx("[0-9]{2}.[0-9]{2}.[0-9]{4}|\
			[0-9]{2}/[0-9]{2}/[0-9]{4}");
	if (!std::regex_match(date, rx))
	{
		throw birthdayException();
	}
}

vector<string> Helper::split(string s, string delimiter) {
	size_t pos_start = 0, pos_end, delim_len = delimiter.length();
	string token;
	vector<string> res;

	while ((pos_end = s.find(delimiter, pos_start)) != string::npos) {
		token = s.substr(pos_start, pos_end - pos_start);
		pos_start = pos_end + delim_len;
		res.push_back(token);
	}

	res.push_back(s.substr(pos_start));
	return res;
}
