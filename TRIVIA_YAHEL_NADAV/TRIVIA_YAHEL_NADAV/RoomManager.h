#pragma once
#include "Room.h"
#include <map>

using std::map;


class RoomManager
{
public:
	RoomManager() = default;
	void createRoom(LoggedUser, RoomData);
	void deleteRoom(string roomName);
	unsigned int getRoomState(string roomName);
	Room getRoom(string roomName);
	Room* getRoomRef(string name);
	vector<Room> getRooms();
	void addUserToRoom(string username, string room);
	void removeUserFromRoom(string username, string room);
	void removeUserFromAllRooms(string username);

private:
	map<string, Room> m_rooms;
};

