#pragma once
#include "IDatabase.h"
#include "LoggedUser.h"
#include "SqliteDateBase.h"
#include <set>
#include <algorithm>
#include "Helper.h"


class LoginManager
{
public:
	~LoginManager() = default;
	static LoginManager* getinstanceLoginManager(IDatabase* db);

	void signup(string username, string password, string mail,
		string address, string phone, string date);
	void login(string username, string password);
	void logout(string username);
	std::set<LoggedUser>& getUsers();

private:
	LoginManager() = default;
	LoginManager(IDatabase * db);

	static LoginManager* instanceLoginManager;

	IDatabase* _m_db;
	std::set<LoggedUser> _m_loggedUser;
};