#include "SqliteDateBase.h"
#include <io.h>
#include <iostream>
#include <vector>
#include "SqlTablesStructs.h"
#include "Helper.h"
#include "json.hpp"
#include "Structs.h"
#include <list>

using std::cout;
using std::endl;
using std::vector;
using nlohmann::json;
using std::list;

SqliteDataBase* SqliteDataBase::instanceSqliteDB = nullptr;

/*
* the function process the data from count queue
*/
int SqliteDataBase::countCallback(void* data, int argc, char** argv, char** azColName)
{
	int* temp = (int*)data;
	for (int i = 0; i < argc; i++)
	{
		if (string(azColName[i]) == "COUNT(*)")
		{
			*temp += atoi(argv[i]);
		}
	}
	return 0;
}

/*
* the function process the data from the sql query
*/
int SqliteDataBase::usersCallBack(void* data, int argc, char** argv, char** azColName)
{
	User user;
	list<User>* users = (list<User>*)data;


	for (int i = 0; i < argc; i++)
	{
		if (string(azColName[i]) == "USERNAME")
		{
			user.username = string(argv[i]);
		}
		if (string(azColName[i]) == "PASSWORD")
		{
			user.password = string(argv[i]);
		}
		if (string(azColName[i]) == "EMAIL")
		{
			user.mail = string(argv[i]);
		}
		if (string(azColName[i]) == "ADDRESS")
		{
			user.address = string(argv[i]);
		}
		if (string(azColName[i]) == "PHONE")
		{
			user.phone = string(argv[i]);
		}
		if (string(azColName[i]) == "BIRTHDAY")
		{
			user.birthday = string(argv[i]);
		}
	}

	users->push_back(user);
	return 0;
}

int SqliteDataBase::statisticsCallBack(void* data, int argc, char** argv, char** azColName)
{
	Statistics stat;
	list<Statistics>* statList = (list<Statistics>*)data;

	for (int i = 0; i < argc; i++)
	{
		if (string(azColName[i]) == "USERNAME")
		{
			stat.username = string(argv[i]);
		}
		if (string(azColName[i]) == "AVERAGE_TIME")
		{
			stat.avgTime = std::stof(argv[i]);
		}
		if (string(azColName[i]) == "TOTAL_ANSWERS")
		{
			stat.totalAnswers = std::stoi(argv[i]);
		}
		if (string(azColName[i]) == "CORRECT_ANSWERS")
		{
			stat.correctAnswers = std::stoi(argv[i]);
		}
		if (string(azColName[i]) == "POINTS")
		{
			stat.points = std::stof(argv[i]);
		}
	}
	statList->push_back(stat);

	return 0;
}


/*
* the function send the sql queue to the database
* input: the sql queue
*/
void SqliteDataBase::sqlAction(string sqlQueue)
{
	try
	{
		char* errMessage = nullptr;
		int result = sqlite3_exec(db, sqlQueue.c_str(), nullptr, nullptr,
			&errMessage);
		if (result != SQLITE_OK)
		{
			throw std::exception();
		}
	}
	catch (const std::exception&)
	{
		cout << "ERROR IN sqlAction" << endl;
	}
}

void SqliteDataBase::insertQuestionsToDB()
{
	string questions = "{\"category\":\"Sports\",\"type\":\"multiple\",\"difficulty\":\
\"easy\",\"question\":\"Which of the following sports is not part of the triathlon ? \",\
\"correct_answer\":\"Horse - Riding\",\"incorrect_answers\":[\"Cycling\",\"Swimming\",\
\"Running\"]}|{\"category\":\"Sports\",\"type\":\"multiple\",\"difficulty\":\"easy\",\
\"question\":\"Who won the 2015 Formula 1 World Championship ? \",\"correct_answer\":\
\"Lewis Hamilton\",\"incorrect_answers\":[\"Nico Rosberg\",\"Sebastian Vettel\",\
\"Jenson Button\"]}|{\"category\":\"Sports\",\"type\":\"multiple\",\"difficulty\":\
\"easy\",\"question\":\"Which driver has been the Formula 1 world champion for a\
record 7 times ? \",\"correct_answer\":\"Michael Schumacher\",\"incorrect_answers\":\
[\"Ayrton Senna\",\"Fernando Alonso\",\"Jim Clark\"]}|{\"category\":\"Sports\",\
\"type\":\"multiple\",\"difficulty\":\"hard\",\"question\":\"Which car company\
is the only Japanese company which won the 24 Hours of Le Mans ? \",\
\"correct_answer\":\"Mazda\",\"incorrect_answers\":[\"Toyota\",\"Subaru\",\
\"Nissan\"]}|{\"category\":\"Sports\",\"type\":\"multiple\",\"difficulty\":\
\"medium\",\"question\":\"Which soccer team won the Copa Am & eacute; rica 2015\
 Championship ? \",\"correct_answer\":\"Chile\",\"incorrect_answers\":\
[\"Argentina\",\"Brazil\",\"Paraguay\"]}|{\"category\":\"Sports\",\"type\":\
\"multiple\",\"difficulty\":\"medium\",\"question\":\"What national team won\
 the 2016 edition of UEFA European Championship ? \",\"correct_answer\":\
\"Portugal\",\"incorrect_answers\":[\"France\",\"Germany\",\"England\"]}|\
{\"category\":\"Sports\",\"type\":\"multiple\",\"difficulty\":\"medium\",\
\"question\":\"Which portuguese island is soccer player Cristiano Ronaldo from ?\
 \",\"correct_answer\":\"Madeira\",\"incorrect_answers\":[\"Terceira\",\
\"Santa Maria\",\"Porto Santo\"]}|{\"category\":\"Sports\",\"type\":\
\"multiple\",\"difficulty\":\"hard\",\"question\":\"Which year was the\
 third Super Bowl held ? \",\"correct_answer\":\"1969\",\
\"incorrect_answers\":[\"1968\",\"1971\",\"1970\"]}|{\"category\":\
\"Sports\",\"type\":\"multiple\",\"difficulty\":\"hard\",\"question\":\
\"Which player & quot; kung - fu kicked & quot; a Crystal Palace fan in\
 January 1995 ? \",\"correct_answer\":\"Eric Cantona\",\"incorrect_answers\":\
[\"David Seamen\",\"Ashley Cole\",\"Mark Hughes\"]}|{\"category\":\"Sports\",\
\"type\":\"multiple\",\"difficulty\":\"easy\",\"question\":\"When was the\
 FC Schalke 04 founded ? \",\"correct_answer\":\"1904\",\
\"incorrect_answers\":[\"1909\",\"2008\",\"1999\"]}";

	vector<string> jsons = Helper::split(questions, "|");
	for (auto question : jsons)
	{
		json qJson = json::parse(question);
		Question q;

		string category = qJson["category"];
		q.category = qJson["category"];

		string type = qJson["type"];
		q.type = qJson["type"];

		string difficulty = qJson["difficulty"];
		q.difficulty = qJson["difficulty"];

		string question = qJson["question"];
		q.question = qJson["question"];

		string correct = qJson["correct_answer"];
		q.correct_answer = qJson["correct_answer"];

		string incorrect1 = qJson["incorrect_answers"][0];
		q.incorrect1 = qJson["incorrect_answers"][0];

		string incorrect2 = qJson["incorrect_answers"][1];
		q.incorrect2 = qJson["incorrect_answers"][1];

		string incorrect3 = qJson["incorrect_answers"][2];
		q.incorrect3 = qJson["incorrect_answers"][2];

		// insert the question to the list of the questions
		this->questions.push_back(q);

		string sqlStatement = "INSERT INTO QUESTIONS (CATEGORY, TYPE,\
		DIFFICULTY, QUESTION, CORRECT_ANSWER, INCORRECT_ANSWER1,\
		 INCORRECT_ANSWER2, INCORRECT_ANSWER3) VALUES(\
		\"" + category + "\",\"" + type + "\",\"" + difficulty + "\",\"" +
		question + "\",\"" + correct + "\",\"" + incorrect1 + "\",\"" +
			incorrect2 + "\",\"" + incorrect3 + "\");";

		sqlAction(sqlStatement);
	}
}


/*
* the function open new/exist Database
*/
bool SqliteDataBase::open()
{
	string dbFileName = "galleryDB.sqlite";

	// check if the file exist
	int doesFileExist = _access(dbFileName.c_str(), 0);

	// open connection
	int result = sqlite3_open(dbFileName.c_str(), &this->db);
	if (result != SQLITE_OK)
	{
		this->db = nullptr;
		cout << "Failed to open DB" << endl;
		return false;
	}


	if (doesFileExist == -1)
	{
		// create the users table
		string sqlStatement = "CREATE TABLE USERS ( \
								USERNAME TEXT PRIMARY KEY NOT NULL,\
								PASSWORD TEXT NOT NULL,\
								EMAIL TEXT NOT NULL,\
								ADDRESS TEXT NOT NULL,\
								PHONE TEXT NOT NULL,\
								BIRTHDAY TEXT NOT NULL);";

		sqlAction(sqlStatement);
		

		// create the questions table
		sqlStatement = "CREATE TABLE QUESTIONS(\
						CATEGORY TEXT NOT NULL,\
						TYPE TEXT NOT NULL,\
						DIFFICULTY TEXT NOT NULL,\
						QUESTION TEXT NOT NULL,\
						CORRECT_ANSWER TEXT NOT NULL,\
						INCORRECT_ANSWER1 TEXT NOT NULL,\
						INCORRECT_ANSWER2 TEXT NOT NULL,\
						INCORRECT_ANSWER3 TEXT NOT NULL);";
		sqlAction(sqlStatement);
		// insert the questions into the db
		insertQuestionsToDB();


		// create the statistics table
		sqlStatement = "CREATE TABLE STATISTICS(\
						USERNAME TEXT NOT NULL,\
						AVERAGE_TIME FLOAT NOT NULL,\
						TOTAL_ANSWERS INT NOT NULL,\
						CORRECT_ANSWERS INT NOT NULL,\
						POINTS FLOAT NOT NULL);";
		sqlAction(sqlStatement);
	}
	return true;
}

/*
* the function close the database
*/
void SqliteDataBase::close()
{
	sqlite3_close(this->db);
	this->db = nullptr;
}

//C'tor.
SqliteDataBase::SqliteDataBase()
{
	open();
}
//D'tor.
SqliteDataBase::~SqliteDataBase()
{
	close();
}

SqliteDataBase* SqliteDataBase::getInstanceSqliteDB()
{
	if (instanceSqliteDB == nullptr)
	{
		instanceSqliteDB = new SqliteDataBase();
	}
	return instanceSqliteDB;
}

/*
* the function check if user is exist
*/
bool SqliteDataBase::doesUserExist(string username)
{
	try
	{
		string sqlQueue = "SELECT COUNT(*) FROM USERS WHERE USERNAME= \"" + username
			+ "\";";

		int counter = 0;

		char* errMessage = nullptr;
		int result = sqlite3_exec(db, sqlQueue.c_str(), countCallback, &counter,
			&errMessage);
		if (result != SQLITE_OK)
		{
			throw std::exception();
		}

		if (counter)
		{
			return true;
		}
		return false;
	}
	catch (const std::exception&)
	{
		cout << "ERROR IN doesUserExist queue" << endl;
	}
}

/*
* the function check if name and password are match
*/
bool SqliteDataBase::doesPasswordMatch(string username, string password)
{
	try
	{
		std::list<User> users;


		string sqlQueue = "SELECT * FROM USERS WHERE PASSWORD= \"" + password
			+ "\";";


		char* errMessage = nullptr;
		int result = sqlite3_exec(db, sqlQueue.c_str(), usersCallBack, &users,
			&errMessage);
		if (result != SQLITE_OK)
		{
			throw std::exception();
		}

		// check if the name and the password are match
		for (auto user : users)
		{
			if (user.username == username && user.password == password)
			{
				return true;
			}
		}
		return false;
	}
	catch (const std::exception&)
	{
		cout << "ERROR IN doesPasswordMatch queue" << endl;
	}
}

/*
* the function add new user record to the database
* input: the username, password and mail of the new user
*/
void SqliteDataBase::addNewUser(string username, string password,
	string mail, string address, string phone, string birthday)
{
	try
	{
		string sqlQueue = "INSERT INTO USERS (USERNAME, PASSWORD, EMAIL,\
		 ADDRESS, PHONE, BIRTHDAY) VALUES(\"" + username + "\", \"" +
			password + "\", \"" + mail + "\", \"" + address + "\",\"" + phone 
			+ "\",\"" + birthday + "\");";

		sqlAction(sqlQueue);
	}
	catch (const std::exception&)
	{
		cout << "ERROR IN addNewUser" << endl;
	}
}

/*
* the function get the average answer time of user
*/
float SqliteDataBase::getPlayerAverageAnswerTime(string username)
{
	list<Statistics> stat;
	
	string sqlStatement = "SELECT * FROM STATISTICS WHERE USERNAME=\"" +
		username + "\";";

	try
	{
		char* errMessage = nullptr;
		int result = sqlite3_exec(db, sqlStatement.c_str(), statisticsCallBack, &stat,
			&errMessage);
		if (result != SQLITE_OK)
		{
			throw std::exception();
		}

		return Helper::getStatisticsAverages(stat).avgTime;
	}
	catch (const std::exception&)
	{
		cout << "ERROR IN getPlayerAverageAnswerTime" << endl;
	}
}

/*
* the function get the number of correct answer
*/
int SqliteDataBase::getNumOfCorrectAnswers(string username)
{
	list<Statistics> stat;

	string sqlStatement = "SELECT * FROM STATISTICS WHERE USERNAME=\"" +
		username + "\";";

	try
	{
		char* errMessage = nullptr;
		int result = sqlite3_exec(db, sqlStatement.c_str(), statisticsCallBack, &stat,
			&errMessage);
		if (result != SQLITE_OK)
		{
			throw std::exception();
		}

		return Helper::getStatisticsAverages(stat).correctAnswers;
	}
	catch (const std::exception&)
	{
		cout << "ERROR IN getNumOfCorrectAnswers" << endl;
	}
}

/*
* the function get the number of total answers
*/
int SqliteDataBase::getNumOfTotalAnswers(string username)
{
	list<Statistics> stat;

	string sqlStatement = "SELECT * FROM STATISTICS WHERE USERNAME=\"" +
		username + "\";";

	try
	{
		char* errMessage = nullptr;
		int result = sqlite3_exec(db, sqlStatement.c_str(), statisticsCallBack, &stat,
			&errMessage);
		if (result != SQLITE_OK)
		{
			throw std::exception();
		}

		return Helper::getStatisticsAverages(stat).totalAnswers;
	}
	catch (const std::exception&)
	{
		cout << "ERROR IN getNumOfTotalAnswers" << endl;
	}
}

/*
* the function get the number of games that the player play
*/
int SqliteDataBase::getNumOfPlayerGames(string username)
{
	int totalGames;

	string sqlStatement = "SELECT COUNT(*) FROM STATISTICS WHERE USERNAME=\"" +
		username + "\";";

	try
	{
		char* errMessage = nullptr;
		int result = sqlite3_exec(db, sqlStatement.c_str(), countCallback, &totalGames,
			&errMessage);
		if (result != SQLITE_OK)
		{
			throw std::exception();
		}

		return totalGames;
	}
	catch (const std::exception&)
	{
		cout << "ERROR IN getNumOfPlayerGames" << endl;
	}
}

list<Question> SqliteDataBase::getQuestions(int num)
{
	return this->questions;
}

/*
* the function get the 5 best scores from the database
*/
vector<string> SqliteDataBase::getBestScore()
{
	vector<string> bestScores;
	list<Statistics> statList;

	string sqlStatement = "SELECT * FROM STATISTICS ORDER\
		 BY POINTS DESC LIMIT 5";

	try
	{
		char* errMessage = nullptr;
		int result = sqlite3_exec(db, sqlStatement.c_str(), statisticsCallBack, &statList,
			&errMessage);
		if (result != SQLITE_OK)
		{
			throw std::exception();
		}

		for (auto stat : statList)
		{
			bestScores.push_back(stat.username + " - " + std::to_string(stat.points));
		}
		return bestScores;
	}
	catch (const std::exception&)
	{
		cout << "ERROR IN getBestScore" << endl;
	}
}
