#pragma once
#include "Structs.h"

struct RequestResult;
struct RequestInfo;

class IRequestHandler
{
public:
	virtual bool isRequestRelevant(RequestInfo) = 0;
	virtual RequestResult handleRequest(RequestInfo) = 0;
};