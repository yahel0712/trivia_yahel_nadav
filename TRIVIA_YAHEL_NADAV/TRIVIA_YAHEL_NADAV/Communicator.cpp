#include "Communicator.h"
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "Structs.h"
#include "LoginRequestsHandler.h"
#include "JsonResponsePacketSerializer.h"
#include "Helper.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <thread>

#define BYTE_SIZE 8

using std::thread;
using std::cout;
using std::endl;


Communicator* Communicator::instanceCommunicator = nullptr;


Communicator::Communicator(RequestHandlerFactory& hf) :
	m_handlerFactory(hf)
{
	// this server use TCP
	m_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (m_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}


Communicator::~Communicator()
{
	try
	{
		closesocket(m_serverSocket);
	}
	catch (...) {}
}


/*
* the function handle the connection with all the clients
*/
void Communicator::startHandlerRequests()
{
	bindAndListen();

	while (true)
	{
		// the main thread is only accepting clients
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}


/*
* the function handle the communication with the client
* input: the socket of the client
*/
void Communicator::handleNewClient(SOCKET client_socket)
{
	IRequestHandler* req =
		this->m_handlerFactory.createLoginRequestHandler();
	while (true)
	{
		try
		{
			int typeCode = 0, jsonLen = 0;
			RequestInfo newReqInfo;


			char m[BYTE_SIZE + 1];
			recv(client_socket, m, BYTE_SIZE, 0);
			m[BYTE_SIZE] = 0;

			// convert the type code from char to int.
			typeCode = ((int)Helper::bytesToChar(string(m)) - '0');

			newReqInfo.id = typeCode;

			// in case of illeagal message type code
			if (req->isRequestRelevant(newReqInfo) == false)
			{
				ErrorResponse er;

				std::vector<unsigned char> buffer;
				buffer = JsonResponsePacketSerializer::serializeResponse(er);

				string msgToSend = "";
				for (auto bit : buffer)
				{
					msgToSend += bit;
				}

				send(client_socket, msgToSend.c_str(), msgToSend.size(), 0);
			}

			// get the len of the msg (json), 4 bytes.
			char getLenMsg[(4 * BYTE_SIZE) + 1];
			recv(client_socket, getLenMsg, 4 * BYTE_SIZE, 0);
			getLenMsg[4 * BYTE_SIZE] = 0;

			// convert the len of the json to int.
			string bytes;
			for (int i = 0; i < 4 * BYTE_SIZE; i++)
			{
				bytes += getLenMsg[i];
			}
			jsonLen = Helper::bytesToInt(bytes);


			char* jsonContent = new char[(jsonLen * BYTE_SIZE) + 1];
			jsonContent[jsonLen * BYTE_SIZE] = 0;

			recv(client_socket, jsonContent, jsonLen * BYTE_SIZE, 0);

			vector <unsigned char> buffer;
			for (int i = 0; i < jsonLen * BYTE_SIZE; i++)
			{
				buffer.push_back(jsonContent[i]);
			}

			newReqInfo.buffer = buffer;
			RequestResult RequestRes = req->handleRequest(newReqInfo);

			string toSend = Helper::convertVectorCharsToString(RequestRes.buffer);

			// send to the client result.
			send(client_socket, toSend.c_str(), toSend.size(), 0);

			req = RequestRes.newHandler;

		}
		catch (std::exception e)
		{
			cout << e.what() << endl;
			break;
		}
	}
}


void Communicator::accept()
{

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(m_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	thread t(&Communicator::handleNewClient, this, client_socket);

	t.detach();
}


void Communicator::bindAndListen()
{
	int port = 7777;
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = INADDR_ANY;

	// Connects between the socket and the configuration
	if (bind(m_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(m_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");

	std::cout << "Listening on port " << port << std::endl;
}

Communicator* Communicator::getInstanceComm(RequestHandlerFactory& hf)
{
	if (instanceCommunicator == nullptr)
	{
		instanceCommunicator = new Communicator(hf);
	}
	return instanceCommunicator;
}

void Communicator::operator=(const Communicator& other)
{
	this->m_serverSocket = other.m_serverSocket;
	this->m_handlerFactory = other.m_handlerFactory;
	this->m_clients = other.m_clients;
}
