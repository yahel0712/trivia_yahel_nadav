#include "LoggedUser.h"
#include <string>

LoggedUser::LoggedUser(string username)
{
    this->_m_username = username;
}

string LoggedUser::getUsername() const
{
    return this->_m_username;
}


bool LoggedUser::operator<(const LoggedUser& other) const
{
	return this->_m_username < other.getUsername();
}

bool LoggedUser::operator==(const LoggedUser& one) const
{
	return this->_m_username == one.getUsername();
}

LoggedUser& LoggedUser::operator=(const LoggedUser other)
{
	this->_m_username = other.getUsername();
	return *this;
}