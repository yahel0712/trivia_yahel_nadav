#include "RoomAdminRequestHandler.h"
#include "JsonResponsePacketSerializer.h"
#include "RequestHandlerFactory.h"


#define CLOSE_ROOM_TYPE_CODE 1
#define START_GAME_TYPE_CODE 2
#define GET_ROOM_STATE_TYPE_CODE 3


RoomAdminRequestHandler::RoomAdminRequestHandler(Room room,
	LoggedUser logUser, RoomManager& roomManager,
	RequestHandlerFactory& ReqHandler) : m_room(room),
	m_roomManager(roomManager), m_handlerFactory(ReqHandler),
	m_user(logUser)
{
}

bool RoomAdminRequestHandler::isRequestRelevant(RequestInfo reqInfo)
{
	return(reqInfo.id == CLOSE_ROOM_TYPE_CODE ||
		reqInfo.id == START_GAME_TYPE_CODE ||
		reqInfo.id == GET_ROOM_STATE_TYPE_CODE);
}

RequestResult RoomAdminRequestHandler::handleRequest(RequestInfo reqinfo)
{
	if (reqinfo.id == GET_ROOM_STATE_TYPE_CODE)
	{
		return getRoomState(reqinfo);
	}
	else if (reqinfo.id == CLOSE_ROOM_TYPE_CODE)
	{
		return closeRoom(reqinfo);
	}
	else if (reqinfo.id == START_GAME_TYPE_CODE)
	{
		return startGame(reqinfo);
	}

	return RequestResult();
}

RequestResult RoomAdminRequestHandler::closeRoom(RequestInfo reqInfo)
{
	CloseRoomResponse crr;
	RequestResult reqRes;
	// send to all the room's users to leave the room.
	vector<string> allUsers = this->m_room.getAllUsers();
	
	this->m_roomManager.deleteRoom(this->m_room.getName());
	crr.status = 1;
	reqRes.buffer = JsonResponsePacketSerializer::serializeResponse(crr);
	reqRes.newHandler = this->m_handlerFactory.createMenuRequestHandler();

	return reqRes;
}

RequestResult RoomAdminRequestHandler::startGame(RequestInfo reqInfo)
{
	RequestResult reqRes;
	StartGameResponse sgr;
	
	this->m_roomManager.getRoomRef(this->m_room.getName())->setHasGameBegan(true);

	sgr.status = 1;
	reqRes.buffer = JsonResponsePacketSerializer::serializeResponse(sgr);
	reqRes.newHandler = this;

	return reqRes;
}

RequestResult RoomAdminRequestHandler::getRoomState(RequestInfo reqInfo)
{
	RequestResult reqRes;
	GetRoomStateResponse grs;
	
	// check if the room exist.
	for (auto i : this->m_roomManager.getRooms())
	{
		if (this->m_room.getName() == i.getName())
		{
			this->m_room = i;

			grs.hasGameBegan = this->m_room.getDataRoom().hasGameBegan;
			grs.status = this->m_room.getDataRoom().isActive;

			
			for (auto user : this->m_room.getAllUsers())
			{
				grs.players += user + ",";
			}
			grs.admin = this->m_room.getDataRoom().admin;

			reqRes.buffer = JsonResponsePacketSerializer::serializeResponse(grs);
			reqRes.newHandler = this;
			return reqRes;
		}
	}
	grs.hasGameBegan = std::to_string(false);
	grs.status = 0;

	reqRes.buffer = JsonResponsePacketSerializer::serializeResponse(grs);
	reqRes.newHandler = this->m_handlerFactory.createMenuRequestHandler();

	return reqRes;
}
