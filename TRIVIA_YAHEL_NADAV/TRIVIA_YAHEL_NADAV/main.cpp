#pragma comment (lib, "ws2_32.lib")

#include "Server.h"
#include "WSAInitializer.h"
#include "Helper.h"

int main()
{
	WSAInitializer wsaInit;

	Server* myServer = Server::getInstanceServer();

	myServer->run();

	return 0;
}