#pragma once
#include "IDatabase.h"
#include <vector>

using std::vector;

class StatisticsManager
{
public:
	StatisticsManager(IDatabase* db);

	vector<string> getHighScore();
	vector<string> getUserStatistics(string username);

private:
	IDatabase* m_database;
};

