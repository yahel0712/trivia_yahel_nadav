#pragma once
#include "IDatabase.h"

class SqliteDataBase : public IDatabase
{
public:
	~SqliteDataBase();

	static SqliteDataBase* getInstanceSqliteDB();

	// user functions
	virtual bool doesUserExist(string username);
	virtual void addNewUser(string username, string password,
		string mail, string address, string phone, string birthday);

	// queries functions
	virtual bool doesPasswordMatch(string username, string password);

	// staticstics functions
	virtual float getPlayerAverageAnswerTime(string username);
	virtual int getNumOfCorrectAnswers(string username);
	virtual int getNumOfTotalAnswers(string username);
	virtual int getNumOfPlayerGames(string username);
	virtual list<Question> getQuestions(int num);
	virtual vector<string> getBestScore();

	// database functions
	bool open() override;
	void close() override;

	// callback functions
	static int countCallback(void* data, int argc,
		char** argv,char** azColName);

	static int usersCallBack(void* data, int argc,
		char** argv, char** azColName);

	static int statisticsCallBack(void* data, int argc,
		char** argv, char** azColName);

	// help functions
	void sqlAction(string sqlQueue);
	void insertQuestionsToDB();

private:
	SqliteDataBase();
	static SqliteDataBase* instanceSqliteDB;
	
	list<Question> questions;

	sqlite3* db;
};

