#pragma once
#include "Structs.h"

class JsonRequestPacketDeserializer
{
public:
	static LoginRequest deserializeLoginRequest(vector<unsigned char> buffer);
	static SignupRequest deserializeSignupRequest(vector<unsigned char> buffer);
	
	static GetPlayersInRoomRequest deserializeGetPlayersInRoomRequest(vector<unsigned char> buffer);
	static JoinRoomRequest deserializeJoinRoomRequest(vector<unsigned char> buffer);
	static CreateRoomRequest deserializeCreateRoomRequest(vector<unsigned char> buffer);
	static LogoutRequest deserializeLogoutRequest(vector<unsigned char> buffer);
	static getRoomRequest deserializeGetRoomRequest(vector<unsigned char> buffer);

	static JsonRequestPacketDeserializer& getInstance();

private:
	static JsonRequestPacketDeserializer* instanceJsonDes;
};

