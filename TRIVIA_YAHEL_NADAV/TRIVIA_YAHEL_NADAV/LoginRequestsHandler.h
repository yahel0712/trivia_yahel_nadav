#pragma once
#include "IRequestHandler.h"
#include "LoginManager.h"
#include "RequestHandlerFactory.h"

class RequestHandlerFactory;


class LoginRequestHandler : public IRequestHandler
{
public:
	LoginRequestHandler(LoginManager& loginManager, RequestHandlerFactory& ReqHandlerFactory);
	
	bool isRequestRelevant(RequestInfo reqInfo);
	RequestResult handleRequest(RequestInfo reqInfo);
	
private:
	RequestResult login(RequestInfo reqInfo);
	RequestResult signup(RequestInfo reqInfo);

	LoginManager& m_loginManager;
	RequestHandlerFactory& m_handlerFactory;
};
