#include "RoomMemberRequestHandler.h"
#include "JsonResponsePacketSerializer.h"
#include "RequestHandlerFactory.h"


#define GET_ROOM_STATE_TYPE_CODE 3
#define LEAVE_ROOM_TYPE_CODE 4


RoomMemberRequestHandler::RoomMemberRequestHandler(Room room,
	LoggedUser logUser, RoomManager& roomManager,
	RequestHandlerFactory& ReqHandler) : m_room(room),
	m_roomManager(roomManager), m_handlerFactory(ReqHandler),
	m_user(logUser)
{
}

bool RoomMemberRequestHandler::isRequestRelevant(RequestInfo reqInfo)
{
	return (reqInfo.id == LEAVE_ROOM_TYPE_CODE ||
		reqInfo.id == GET_ROOM_STATE_TYPE_CODE);
}

RequestResult RoomMemberRequestHandler::handleRequest(RequestInfo reqinfo)
{
	if (reqinfo.id == GET_ROOM_STATE_TYPE_CODE)
	{
		return getRoomState(reqinfo);
	}
	else if (reqinfo.id == LEAVE_ROOM_TYPE_CODE)
	{
		return leaveRoom(reqinfo);
	}

	RequestResult reqRes;
	reqRes.buffer = JsonResponsePacketSerializer::serializeResponse(ErrorResponse());
	reqRes.newHandler = this;

	return reqRes;
}


RequestResult RoomMemberRequestHandler::leaveRoom(RequestInfo reqInfo)
{
	LeaveRoomResponse lrr;
	RequestResult reqRes;

	this->m_roomManager.removeUserFromRoom(this->m_user.getUsername(),
		this->m_room.getName());
	lrr.status = 1;
	reqRes.buffer = JsonResponsePacketSerializer::serializeResponse(lrr);
	reqRes.newHandler = this->m_handlerFactory.createMenuRequestHandler();

	return reqRes;
}

RequestResult RoomMemberRequestHandler::getRoomState(RequestInfo reqInfo)
{
	RequestResult reqRes;
	GetRoomStateResponse grs;
	bool userInRoom = false;

	// check if the room exist.
	for (auto i : this->m_roomManager.getRooms())
	{
		if (this->m_room.getName() == i.getName())
		{

			// refresh the toom states.
			this->m_room = i;

			grs.hasGameBegan = std::to_string(i.getDataRoom().hasGameBegan);
			grs.status = i.getDataRoom().isActive;


			for (auto user : this->m_room.getAllUsers())
			{
				grs.players += user + ",";
			}
			grs.admin = i.getDataRoom().admin;

			reqRes.buffer = JsonResponsePacketSerializer::serializeResponse(grs);
			reqRes.newHandler = this;
			return reqRes;
		}
	}
	grs.hasGameBegan = std::to_string(false);
	grs.status = 0;

	reqRes.buffer = JsonResponsePacketSerializer::serializeResponse(grs);
	reqRes.newHandler = this->m_handlerFactory.createMenuRequestHandler();;
}
