#pragma once
#include <vector>
#include <string>
#include "LoggedUser.h"
#include "Structs.h"

using std::string;
using std::vector;


class Room
{
public:
	Room(RoomData data, LoggedUser logUser);

	void addUser(LoggedUser logUser);
	void removeUser(LoggedUser logUser);

	vector<string> getAllUsers();
	RoomData getDataRoom();
	string getName();
	vector<LoggedUser> getUsers();
	
	void setActiveRoom(unsigned int active);
	void setHasGameBegan(bool ifGameBegan);
private:
	RoomData m_metadata;
	vector<LoggedUser> m_users;
};

