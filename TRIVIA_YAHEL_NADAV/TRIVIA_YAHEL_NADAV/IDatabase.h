#pragma once
#include "sqlite3.h"
#include <io.h>
#include <string>
#include "Structs.h"
#include <list>

using std::list;
using std::string;

class IDatabase
{
public:
	IDatabase() = default;
	virtual ~IDatabase() = default;

	virtual bool doesUserExist(string username) = 0;
	virtual bool doesPasswordMatch(string username,
		string password) = 0;
	virtual void addNewUser(string username, string password,
		string mail, string address, string phone, string birthday) = 0;
	virtual float getPlayerAverageAnswerTime(string username) = 0;
	virtual int getNumOfCorrectAnswers(string username) = 0;
	virtual int getNumOfTotalAnswers(string username) = 0;
	virtual int getNumOfPlayerGames(string username) = 0;
	virtual list<Question> getQuestions(int num) = 0;
	virtual vector<string> getBestScore() = 0;

	virtual bool open() = 0;
	virtual void close() = 0;
};

