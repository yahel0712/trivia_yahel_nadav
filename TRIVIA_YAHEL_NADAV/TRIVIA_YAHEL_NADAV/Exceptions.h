#pragma once
#include <exception>

using std::exception;

struct passwordException : public exception
{
	const char* what() const throw()
	{
		return "Error in password input";
	}
};

struct emailException : public exception
{
	const char* what() const throw()
	{
		return "Error in email input";
	}
};

struct onlineUserException : public exception
{
	const char* what() const throw()
	{
		return "Error - The user is online";
	}
};

struct addressException : public exception
{
	const char* what() const throw()
	{
		return "Error in address input";
	}
};

struct phoneException : public exception
{
	const char* what() const throw()
	{
		return "Error in phone input";
	}
};

struct birthdayException : public exception
{
	const char* what() const throw()
	{
		return "Error in birthday input";
	}
};

struct matchException : public exception
{
	const char* what() const throw()
	{
		return "Error - username and password aren't match";
	}
};

struct userNotExistException : public exception
{
	const char* what() const throw()
	{
		return "Error - user isn't exist";
	}
};

struct userAlreadyExistException : public exception
{
	const char* what() const throw()
	{
		return "Error - user already exist.";
	}
};

struct questionCountException : public exception
{
	const char* what() const throw()
	{
		return "Error - the question count must to be between one to ten.";
	}
};

struct answerTimeoutException : public exception
{
	const char* what() const throw()
	{
		return "Error - Time to answer must to be positive.";
	}
};

struct maxUsersException : public exception
{
	const char* what() const throw()
	{
		return "Error - max users must be greater than one.";
	}
};

struct joinRoomException : public exception
{
	const char* what() const throw()
	{
		return "The room isn't exist - join failed";
	}
};

struct exitRoomException : public exception
{
	const char* what() const throw()
	{
		return "The user isn't exist in the room";
	}
};

struct MaxPlayersInRoomException : public exception
{
	const char* what() const throw()
	{
		return "The room is full.";
	}
};
