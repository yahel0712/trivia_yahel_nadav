#pragma once
#include <string>
#include "LoggedUser.h"

using std::string;

class LoggedUser
{
public:
	LoggedUser() = default;
	LoggedUser(string username);
	~LoggedUser() = default;
	string getUsername() const;


	bool operator<( const LoggedUser& other) const;
	bool operator==(const LoggedUser& one) const;
	LoggedUser& operator=(const LoggedUser other);

private:
	string _m_username;
};

