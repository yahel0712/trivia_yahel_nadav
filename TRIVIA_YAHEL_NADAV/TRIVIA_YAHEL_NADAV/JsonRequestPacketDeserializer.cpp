#include "JsonRequestPacketDeserializer.h"
#include "Helper.h"
#include "json.hpp"
#include "Exceptions.h"

#define BYTE_SIZE 8

using std::cout;
using nlohmann::json;

LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(vector<unsigned char> buffer)
{
	string full_msg = "";
	json jsonToConvert;
	LoginRequest loginReq;

	// convert the buffer data to string (from bytes to chars)
	full_msg = Helper::allBytesToString(buffer);

	// convert the data from string to json.
	jsonToConvert = json::parse(full_msg);

	// convert the json to struct of loginRequest.
	loginReq.password = jsonToConvert["password"];
	loginReq.username = jsonToConvert["username"];

	return loginReq;
}



SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(vector<unsigned char> buffer)
{
	string full_msg = "";
	json jsonToConvert;
	SignupRequest signupReq;

	// convert the buffer data to string (from bytes to chars).
	full_msg = Helper::allBytesToString(buffer);

	// convert the data from string to json.
	jsonToConvert = json::parse(full_msg);

	// convert the json to struct of signupRequest.
	signupReq.password = jsonToConvert["password"];
	signupReq.username = jsonToConvert["username"];
	signupReq.email = jsonToConvert["mail"];
	signupReq.address = jsonToConvert["address"];
	signupReq.phone = jsonToConvert["phone"];
	signupReq.birthday = jsonToConvert["birthday"];

	return signupReq;
}

GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(vector<unsigned char> buffer)
{
	string full_msg = "";
	json jsonToConvert;
	GetPlayersInRoomRequest getPlayersInRoom;

	// convert the buffer data to string (from bytes to chars).
	full_msg = Helper::allBytesToString(buffer);

	// convert the data from string to json.
	jsonToConvert = json::parse(full_msg);

	// convert the json to struct of GetPlayersInRoomRequest.
	getPlayersInRoom.roomName = jsonToConvert["roomName"];

	return getPlayersInRoom;
}

JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(vector<unsigned char> buffer)
{
	string full_msg = "";
	json jsonToConvert;
	JoinRoomRequest joinRoomReq;

	// convert the buffer data to string (from bytes to chars).
	full_msg = Helper::allBytesToString(buffer);

	// convert the data from string to json.
	jsonToConvert = json::parse(full_msg);

	// convert the json to struct of GetPlayersInRoomRequest.
	joinRoomReq.roomName = jsonToConvert["roomName"];
	joinRoomReq.username = jsonToConvert["username"];

	return joinRoomReq;
}

LogoutRequest JsonRequestPacketDeserializer::deserializeLogoutRequest(vector<unsigned char> buffer)
{
	string full_msg = "";
	json jsonToConvert;
	LogoutRequest logoutReq;

	// convert the buffer data to string (from bytes to chars).
	full_msg = Helper::allBytesToString(buffer);

	// convert the data from string to json.
	jsonToConvert = json::parse(full_msg);


	// convert the json to struct of LogoutRequest.
	logoutReq.userName = jsonToConvert["userName"];
	

	return logoutReq;
}

getRoomRequest JsonRequestPacketDeserializer::deserializeGetRoomRequest(vector<unsigned char> buffer)
{
	string full_msg = "";
	json jsonToConvert;
	getRoomRequest grr;

	// convert the buffer data to string (from bytes to chars).
	full_msg = Helper::allBytesToString(buffer);

	// convert the data from string to json.
	jsonToConvert = json::parse(full_msg);

	// convert the json to struct of LogoutRequest.
	grr.roomName = jsonToConvert["roomName"];

	return grr;
}

CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(vector<unsigned char> buffer)
{
	string full_msg = "";
	json jsonToConvert;
	CreateRoomRequest createRoomReq;

	// convert the buffer data to string (from bytes to chars).
	full_msg = Helper::allBytesToString(buffer);

	// convert the data from string to json.
	jsonToConvert = json::parse(full_msg);

	// convert the json to struct of GetPlayersInRoomRequest.
	createRoomReq.roomName = jsonToConvert["roomName"];
	createRoomReq.questionCount = jsonToConvert["questionCount"];
	createRoomReq.maxUsers = jsonToConvert["maxUsers"];
	createRoomReq.answerTimeout = jsonToConvert["answerTimeout"];
	createRoomReq.admin = jsonToConvert["admin"];

	if (createRoomReq.answerTimeout <= 0)
	{
		throw answerTimeoutException();
	}
	else if (createRoomReq.maxUsers <= 1)
	{
		throw maxUsersException();
	}
	else if (createRoomReq.questionCount < 1 ||
		createRoomReq.questionCount > 10)
	{
		throw questionCountException();
	}


	return createRoomReq;
}
