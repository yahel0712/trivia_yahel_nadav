#pragma once
#include "IRequestHandler.h"
#include <iostream>
#include <string>
#include <ctime>
#include <exception>
#include <vector>
class IRequestHandler;

using std::string;
using std::vector;
using std::string;
using std::exception;

struct RoomData
{
	int id;
	string name;
	string admin;
	unsigned int maxPlayers;
	unsigned int timePerQuestion;
	unsigned int numOfQuestionInGame;
	unsigned int isActive = 1;
	bool hasGameBegan = false;
};

struct LoginRequest
{
	string username;
	string password;
};

struct SignupRequest
{
	string username;
	string password;
	string email;
	string address;
	string phone;
	string birthday;
};

struct RequestInfo
{
	int id;
	time_t receivalTime;
	vector <unsigned char> buffer;
};

struct RequestResult
{
	vector <unsigned char> buffer;
	IRequestHandler* newHandler;
};

struct LoginResponse
{
	unsigned int status = 1;
	const char msgCode = '1';
};

struct SignupResponse
{
	unsigned int status = 1;
	const char msgCode = '2';
};

struct ErrorResponse
{
	std::string message;
	const char msgCode = '0';
};

struct Statistics
{
	std::string username;
	float avgTime;
	int totalAnswers;
	int correctAnswers;
	float points;
};

struct Question
{
	string category;
	string type;
	string difficulty;
	string question;
	string correct_answer;
	string incorrect1;
	string incorrect2;
	string incorrect3;
};

struct LogoutResponse
{
	int status = 1;
	const char msgCode = '3';
};

struct JoinRoomResponse
{
	int status = 1;
	const char msgCode = '4';
};

struct CreateRoomResponse
{
	int status = 1;
	const char msgCode = '5';
};

struct  getRoomResponse
{
	const char msgCode = '9';
	string name;
	string admin;
	string users;
};

struct getPlayersInRoomResponse
{
	const char msgCode = '7';
	string users;
};

struct HighScoreResponse
{
	const char msgCode = '8';
	string message;
};

struct GetPlayersInRoomRequest
{
	unsigned int roomId;
	string roomName;
};

struct JoinRoomRequest
{
	string username;
	string roomName;
};

struct CreateRoomRequest
{
	string roomName;
	string admin;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
};

struct LogoutRequest
{
	string userName;
};

struct getRoomsResponse
{
	const char msgCode = '6';
	string rooms;
};

struct getRoomRequest
{
	string roomName;
};

struct CloseRoomResponse
{
	const char msgCode = '1';
	unsigned int status;
};

struct StartGameResponse
{
	const char msgCode = '2';
	unsigned int status;
};

struct LeaveRoomResponse
{
	const char msgCode = '4';
	unsigned int status;
};

struct GetRoomStateResponse
{
	const char msgCode = '3';
	unsigned int status;
	string hasGameBegan;
	string players;
	string admin;
};