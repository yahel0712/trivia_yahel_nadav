#pragma once
#include "IRequestHandler.h"
#include "LoggedUser.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include <set>


class RequestHandlerFactory;

class MenuRequestHandler : public IRequestHandler
{
public:
	MenuRequestHandler() = default;
	~MenuRequestHandler() = default;
	MenuRequestHandler(std::set<LoggedUser>& users, RoomManager& roomManager,
		StatisticsManager& statisticsManager, RequestHandlerFactory& handlerFactory);
	bool isRequestRelevant(RequestInfo reqInfo);
	RequestResult handleRequest(RequestInfo reqInfo);

private:
	RequestResult signout(RequestInfo reqInfo);
	RequestResult getRooms(RequestInfo reqInfo);
	RequestResult getPlayersInRoom(RequestInfo reqInfo);
	RequestResult getRoom(RequestInfo reqInfo);
	//RequestResult getPersonalStats(RequestInfo reqInfo);
	//RequestResult getHighScore(RequestInfo reqInfo);
	RequestResult joinRoom(RequestInfo reqInfo);
	RequestResult createRoom(RequestInfo reqInfo);

	std::set<LoggedUser>& m_users;
	RoomManager& m_roomManager;
	StatisticsManager& m_statisticsManager;
	RequestHandlerFactory& m_handlerFactory;
};

