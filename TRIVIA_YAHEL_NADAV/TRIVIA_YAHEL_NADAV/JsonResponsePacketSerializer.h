#pragma once
#include <vector>

#include <iostream>
#include <string>
#include <cstdint>
#include <bitset>
#include <sstream>
#include "Structs.h"
#include "Helper.h"
#include "json.hpp"

using json = nlohmann::json;
using std::vector;
using std::string;

class JsonResponsePacketSerializer
{
public:
	static vector<unsigned char> SerializerResponse(string msgContent,
		char msgCode);
	static vector<unsigned char> serializeResponse(ErrorResponse er);
	static vector<unsigned char> serializeResponse(LoginResponse er);
	static vector<unsigned char> serializeResponse(SignupResponse er);
	static vector<unsigned char> serializeResponse(LogoutResponse er);
	static vector<unsigned char> serializeResponse(getRoomsResponse er);
	static vector<unsigned char> serializeResponse(getRoomResponse er);
	static vector<unsigned char> serializeResponse(getPlayersInRoomResponse er);
	static vector<unsigned char> serializeResponse(JoinRoomResponse er);
	static vector<unsigned char> serializeResponse(CreateRoomResponse er);
	static vector<unsigned char> serializeResponse(HighScoreResponse er);
	static vector<unsigned char> serializeResponse(CloseRoomResponse er);
	static vector<unsigned char> serializeResponse(StartGameResponse er);
	static vector<unsigned char> serializeResponse(LeaveRoomResponse er);
	static vector<unsigned char> serializeResponse(GetRoomStateResponse er);
};

