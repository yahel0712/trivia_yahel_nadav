#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include "Structs.h"

using std::string;
using std::vector;
using std::list;

class Helper
{
public:
	static string allBytesToString(vector<unsigned char> buffer);
	static char bytesToChar(string bytesToConvert);
	static string numberOfBytesToString(vector<unsigned char> buffer, int numberOfBytes);
	static string charToBytes(char charToConvert);
	static void insertStringIntoCharVector(vector<unsigned char>& charV,
		string stringToInsert);
	static int bytesToInt(string bytesToConvert);
	static string convertVectorCharsToString(vector<unsigned char> buffer);

	// statistics helpers
	static Statistics getStatisticsAverages(list<Statistics> statList);

	// validate functoins
	static void validPassword(string password);
	static void validMail(string mail);
	static void validAddress(string address);
	static void validPhone(string phone);
	static void validDate(string date);
	static vector<string> split(string s, string delimiter);
};

