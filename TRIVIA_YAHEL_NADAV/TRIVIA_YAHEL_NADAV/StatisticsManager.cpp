#include "StatisticsManager.h"


StatisticsManager::StatisticsManager(IDatabase* db):
	m_database(db)
{
}

vector<string> StatisticsManager::getHighScore()
{
	return this->m_database->getBestScore();
}

vector<string> StatisticsManager::getUserStatistics(string username)
{
	vector<string> userstats;

	// push all the stats to the vector of the stats.
	userstats.push_back(std::to_string(this->m_database->getPlayerAverageAnswerTime(username)));
	userstats.push_back(std::to_string(this->m_database->getNumOfTotalAnswers(username)));
	userstats.push_back(std::to_string(this->m_database->getNumOfPlayerGames(username)));
	userstats.push_back(std::to_string(this->m_database->getNumOfCorrectAnswers(username)));

	return userstats;
}
