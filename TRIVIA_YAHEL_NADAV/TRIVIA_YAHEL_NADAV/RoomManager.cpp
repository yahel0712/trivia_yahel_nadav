#include "RoomManager.h"
#include "Exceptions.h"

void RoomManager::createRoom(LoggedUser logUser, RoomData roomData)
{
	Room newRoom(roomData, logUser);

	this->m_rooms.insert(std::pair<string, Room>(roomData.name, newRoom));
}

void RoomManager::deleteRoom(string roomName)
{
	auto it = this->m_rooms.find(roomName);

	// check if the room exist.
	if (it == this->m_rooms.end())
	{
		std::cout << "Not found, there is no room with "
			<< roomName << "name" << std::endl;
	}
	else
	{
		this->m_rooms.erase(roomName);
	}
}

unsigned int RoomManager::getRoomState(string roomName)
{
	auto it = this->m_rooms.find(roomName);

	// check if the room exist.
	if (it == this->m_rooms.end())
	{
		std::cout << "Not found, there is no room with "
			<< roomName << "name" << std::endl;
	}

	return it->second.getDataRoom().isActive;
}

Room RoomManager::getRoom(string roomName)
{
	for (auto room : this->m_rooms)
	{
		if (room.second.getName() == roomName)
		{
			return room.second;
		}
	}
}

Room* RoomManager::getRoomRef(string name)
{
	for (auto &r : this->m_rooms)
	{
		if (r.second.getName() == name)
		{
			return &(r.second);
		}
	}
}

vector<Room> RoomManager::getRooms()
{
	vector<Room> allRooms;

	for (auto it : this->m_rooms)
	{
		allRooms.push_back(it.second);
	}
	return allRooms;
}

void RoomManager::addUserToRoom(string username, string room)
{
	bool flag = false;
	for (auto &r : this->m_rooms)
	{
		if (r.second.getName() == room)
		{
			r.second.addUser(LoggedUser(username));
			flag = true;
		}
	}
	if (!flag)
	{
		throw joinRoomException();
	}
}

void RoomManager::removeUserFromRoom(string username, string room)
{
	bool flag = false;
	for (auto &r : this->m_rooms)
	{
		if (r.second.getName() == room)
		{
			r.second.removeUser(LoggedUser(username));
			flag = true;
		}
	}
	if (!flag)
	{
		throw exitRoomException();
	}
}

void RoomManager::removeUserFromAllRooms(string username)
{
	for (auto &r : this->m_rooms)
	{
		for (auto &user : r.second.getUsers())
		{
			if (username == user.getUsername())
			{
				r.second.removeUser(LoggedUser(username));
			}
		}
	}
}
