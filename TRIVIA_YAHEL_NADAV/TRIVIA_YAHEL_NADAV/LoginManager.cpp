#include "LoginManager.h"
#include "Exceptions.h"
#include <iostream>

LoginManager* LoginManager::instanceLoginManager = nullptr;


// to singleton class.
LoginManager* LoginManager::getinstanceLoginManager(IDatabase* db)
{
	if (instanceLoginManager == nullptr)
	{
		instanceLoginManager = new LoginManager(db);
	}
	return instanceLoginManager;
}

LoginManager::LoginManager(IDatabase* db)
{
	this->_m_db = db;
}

/*
* the function add the user to the set of the logged users
*/
void LoginManager::login(string username, string password)
{
	if (this->_m_db->doesUserExist(username))
	{
		if (this->_m_loggedUser.find(LoggedUser(username)) == this->_m_loggedUser.end())
		{
			if (this->_m_db->doesPasswordMatch(username, password))
			{
				this->_m_loggedUser.insert(LoggedUser(username));
			}
			else
			{
				throw matchException();
			}
		}
		else
		{
			throw onlineUserException();
		}
	}
	else
	{
		throw userNotExistException();
	}
}

/*
* the function get user's data and add the user to the database
*/
void LoginManager::signup(string username, string password, string mail,
	string address, string phone, string date)
{
	if (!this->_m_db->doesUserExist(username))
	{
		Helper::validPassword(password);
		Helper::validMail(mail);
		Helper::validAddress(address);
		Helper::validPhone(phone);
		Helper::validDate(date);
	
		this->_m_db->addNewUser(username, password, mail, address, phone, date);

	}
	else
	{
		throw userAlreadyExistException();
	}
}


/*
* the function remove user from the set of the logged users
*/
void LoginManager::logout(string username)
{
	std::set<LoggedUser>::iterator it =
	this->_m_loggedUser.find(LoggedUser(username));

	if (it != this->_m_loggedUser.end())
	{
		this->_m_loggedUser.erase(it);
	}
	else
	{
		throw userNotExistException();
	}
}

std::set<LoggedUser>& LoginManager::getUsers()
{
	return this->_m_loggedUser;
}
