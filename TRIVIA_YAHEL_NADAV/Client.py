import socket
import json
from textwrap import wrap

SERVER_PORT = 7777
INT_BYTES = 4


class Login:
    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.msg_code = '1'


class Signup:
    def __init__(self, username, password, mail, address,
                 phone, birthday):
        self.username = username
        self.password = password
        self.mail = mail
        self.address = address
        self.phone = phone
        self.birthday = birthday
        self.msg_code = '2'


def get_login():
    """
    the function build login request and return the msg with the msg
    code, the length of the content and the content as string of bytes
    """
     # Login
    login_request = Login(input("Enter username:"),
                          input("Enter password (8 chars including small&capital letters, digits and special chars)\nfor example: Aa123!@#) :"))

    login_json = {"username" : login_request.username,
                  "password" : login_request.password
                  }

    # convert the json into string
    json_string = json.dumps(login_json)

    # get the length of the msg content
    content_length = str(len(json_string))
    content_length = str.zfill(content_length, INT_BYTES)

    # build the full msg that sent to the server
    full_msg = login_request.msg_code + content_length + json_string

    return string_to_binary(full_msg)


def get_signup():
    """
    the function build the signup request and return the msg with the
    msg code, the length of the content and the content as string
    of bytes
    """
     # signup

    signup_request = Signup(input("Enter username:"),
                          input("Enter password (8 chars including small&capital letters, digits and special chars)\nfor example: Aa123!@#) :"),
                            input("Enter mail (for example: moshe123@gmail.com) :"),
                            input("Enter address (Street(letters), house number(digits), City(letters)\nfor example:"
                                  "Hertzel, 111, Ashdod) :"),
                            input("Enter phone number ( 0 and 8 or 9 digits after (only digits)\nfor example: 0521234567 or 081234567 ) :"),
                            input("Enter birthday ( day(2 digits).month(2 digits).year(4 digits) or day(2 digits)/month(2 digits)/year(4 digits))\nfor example 01.01.2000 or 01/01/2000:"))

    signup_json = {"username" : signup_request.username,
                   "password" : signup_request.password,
                   "mail" : signup_request.mail,
                   "address" : signup_request.address,
                   "phone" : signup_request.phone,
                   "birthday" : signup_request.birthday}

     # convert the json into string
    json_string = json.dumps(signup_json)

    # get the length of the msg content
    content_length = str(len(json_string))
    content_length = str.zfill(content_length, INT_BYTES)

    # build the full msg that sent to the server
    full_msg = signup_request.msg_code + content_length + json_string

    return string_to_binary(full_msg)


def string_to_binary(string):
    """
    the function get string and convert to binary
    :type string: str
    :param string: the string to convert
    :return: the binary string
    """
    # Converting String to binary
    return ''.join(format(ord(i), '08b') for i in string)


def main():
    choice = 1

    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Connecting to remote computer
    server_address = ("127.0.0.1", SERVER_PORT)
    sock.connect(server_address)

    while(choice != 0):
        choice = int(input("Enter 1 for Login or 2 for signup or 0 to exit:"))
        if(choice == 1):
            # send login request
            msg = get_login()
            sock.sendall(msg.encode())
        elif(choice == 2):
            # send signup request
            msg = get_signup()
            sock.sendall(msg.encode())
        if(choice != 0):
            # get answer from the server
            msg = sock.recv(1024).decode()

            split_msg = wrap(msg, 8)
            msg_in_chars = ""
            for bit in split_msg:
                temp = int(bit, 2)
                char = chr(temp)
                msg_in_chars += char

            print("\nserver response:  " + msg_in_chars)
    

    sock.close()


if __name__ == '__main__':
    main()
