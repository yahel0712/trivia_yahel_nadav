﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows;

namespace GUI_WORKSHOP2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class login : Window
    {
        private string username;
        private string password;

        public login()
        {
            InitializeComponent();
        }
        public login(string username, string password)
        {
            InitializeComponent();
            this.username = username;
            this.password = password;

            this.name.AppendText(username);
            this.psw.AppendText(password);
        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            if (name.Text == "" || psw.Text == "")
            {
                MessageBox.Show("Username/Password can't be empty");
            }
            else
            {
                char msgCode = '1';
                string fullMsg = "{\"username\" : \"" + name.Text + "\", \"password\" : \"" +
                    psw.Text + "\"}";
                int msgLength = fullMsg.Length;

                var msgLengthBytes = msgLength.ToString("D4");

                fullMsg = msgCode.ToString() + msgLengthBytes + fullMsg;
                var b = string.Join("", Encoding.UTF8.GetBytes(fullMsg).Select(n => Convert.ToString(n, 2).PadLeft(8, '0')));
                Byte[] vs = new byte[b.Length];
                for (int i = 0; i < b.Length; i++)
                {
                    vs[i] = Convert.ToByte(b[i]);
                }

                try
                {
                    IPAddress ipAddress = IPAddress.Parse("127.0.0.1");
                    IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 7777);

                    // Create a TCP/IP  socket.  
                    Socket client = new Socket(ipAddress.AddressFamily,
                        SocketType.Stream, ProtocolType.Tcp);

                    client.Connect(remoteEP);

                    //byte[] msgInBytes = new ASCIIEncoding().GetBytes(fullMsg);
                    ////sending the signup request
                    client.Send(vs);
                    //from now - enditing
                    var answer = new byte[1026];
                    int bytesRead = client.Receive(answer);

                    string str = Encoding.Default.GetString(answer);
                    str = str.Replace("\0", String.Empty);
                    byte[] bytes = new byte[str.Length / 8];
                    int j = 0;
                    while (str.Length > 0)
                    {
                        var result = Convert.ToByte(str.Substring(0, 8), 2);
                        bytes[j++] = result;
                        if (str.Length >= 8)
                            str = str.Substring(8);
                    }
                    var resultString = Encoding.UTF8.GetString(bytes);

                    //now lets parse answer to know if we are good
                    string[] msgSplit = resultString.Split(':');

                    if (msgSplit[1].Contains("1"))
                    {
                        MessageBox.Show("User Login Successfully!", "Welcome", MessageBoxButton.OK);

                        // redirect to the main menu                        
                        var MainMenuPage = new users_menu(this.name.Text, client);
                        this.Close();
                        MainMenuPage.Show();
                    }
                    else
                    {
                        string errorMsg = msgSplit[1].Replace("}", string.Empty);
                        errorMsg = errorMsg.Replace("\"", string.Empty);
                        MessageBox.Show(errorMsg, "Failed", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
            }
        }

        private void signupB_Click(object sender, RoutedEventArgs e)
        {
            var signupWindow = new signup();
            signupWindow.Show();
            this.Close();
        }

    }
}
