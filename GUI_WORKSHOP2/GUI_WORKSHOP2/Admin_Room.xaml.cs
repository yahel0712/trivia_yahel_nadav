﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUI_WORKSHOP2
{
    /// <summary>
    /// Interaction logic for Admin_Room.xaml
    /// </summary>
    public partial class Admin_Room : Window
    {
        private Socket client;
        private string roomName;
        private string username;
        private volatile bool flag = true;
        private bool isClose = true;


        private void getRoomState()
        {
            while (flag)
            {
                // wait 2 seconds till update again
                Thread.Sleep(TimeSpan.FromSeconds(2));
                this.Dispatcher.BeginInvoke(new Action(() =>
                {
                    char msgCode = '3';
                    string fullMsg = "{}";
                    int msgLength = fullMsg.Length;

                    var msgLengthBytes = msgLength.ToString("D4");

                    fullMsg = msgCode.ToString() + msgLengthBytes + fullMsg;
                    var b = string.Join("", Encoding.UTF8.GetBytes(fullMsg).Select(n => Convert.ToString(n, 2).PadLeft(8, '0')));
                    Byte[] vs = new byte[b.Length];
                    for (int i = 0; i < b.Length; i++)
                    {
                        vs[i] = Convert.ToByte(b[i]);
                    }

                    try
                    {
                        client.Send(vs);
                        var answer = new byte[1026];
                        int bytesRead = client.Receive(answer);

                        string str = Encoding.Default.GetString(answer);
                        str = str.Replace("\0", String.Empty);
                        byte[] bytes = new byte[str.Length / 8];
                        int j = 0;
                        while (str.Length > 0)
                        {
                            var result = Convert.ToByte(str.Substring(0, 8), 2);
                            bytes[j++] = result;
                            if (str.Length >= 8)
                                str = str.Substring(8);
                        }
                        var resultString = Encoding.UTF8.GetString(bytes);

                        //now lets parse answer to know if we are good
                        string[] msgSplit = resultString.Split('{');

                        // get the rooms data
                        string jsonString = "{" + msgSplit[1];

                        // parse the json string to json object
                        JObject o = JObject.Parse(jsonString);

                        room_Name.Text = this.roomName;
                        adminName.Text = (string)o["admin"];
                        users.Text = (string)o["players"];

                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show(exc.Message);
                    }
                }));
            }
        }

        public Admin_Room(Socket client, string roomName, string username)
        {
            InitializeComponent();
            this.client = client;
            this.roomName = roomName;
            this.username = username;

            Thread t = new Thread(getRoomState);
            t.IsBackground = true;
            t.Start();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            flag = false;
            // check if the X button clicked
            if (isClose)
            {

                // send close room request to the server
                char msgCode = '1';
                string fullMsg = "{}";
                int msgLength = fullMsg.Length;

                var msgLengthBytes = msgLength.ToString("D4");

                fullMsg = msgCode.ToString() + msgLengthBytes + fullMsg;
                var b = string.Join("", Encoding.UTF8.GetBytes(fullMsg).Select(n => Convert.ToString(n, 2).PadLeft(8, '0')));
                Byte[] vs = new byte[b.Length];
                for (int i = 0; i < b.Length; i++)
                {
                    vs[i] = Convert.ToByte(b[i]);
                }

                try
                {
                    client.Send(vs);
                    var answer = new byte[1026];
                    int bytesRead = client.Receive(answer);

                    string str = Encoding.Default.GetString(answer);
                    str = str.Replace("\0", String.Empty);
                    byte[] bytes = new byte[str.Length / 8];
                    int j = 0;
                    while (str.Length > 0)
                    {
                        var result = Convert.ToByte(str.Substring(0, 8), 2);
                        bytes[j++] = result;
                        if (str.Length >= 8)
                            str = str.Substring(8);
                    }
                    var resultString = Encoding.UTF8.GetString(bytes);

                    //now lets parse answer to know if we are good
                    string[] msgSplit = resultString.Split('{');

                    // get the rooms data
                    string jsonString = "{" + msgSplit[1];

                    if (jsonString.Contains("1"))
                    {
                        MessageBox.Show("Closing the room...");

                        isClose = false;
                        this.Close();
                    }

                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }


                // send logout request to the server
                msgCode = '3';
                fullMsg = "{\"userName\" : \"" + username + "\"}";
                msgLength = fullMsg.Length;

                msgLengthBytes = msgLength.ToString("D4");

                fullMsg = msgCode.ToString() + msgLengthBytes + fullMsg;
                b = string.Join("", Encoding.UTF8.GetBytes(fullMsg).Select(n => Convert.ToString(n, 2).PadLeft(8, '0')));
                vs = new byte[b.Length];
                for (int i = 0; i < b.Length; i++)
                {
                    vs[i] = Convert.ToByte(b[i]);
                }

                try
                {
                    ////sending the signup request
                    client.Send(vs);
                    //from now - enditing
                    var answer = new byte[1026];
                    int bytesRead = client.Receive(answer);

                    string str = Encoding.Default.GetString(answer);
                    str = str.Replace("\0", String.Empty);
                    byte[] bytes = new byte[str.Length / 8];
                    int j = 0;
                    while (str.Length > 0)
                    {
                        var result = Convert.ToByte(str.Substring(0, 8), 2);
                        bytes[j++] = result;
                        if (str.Length >= 8)
                            str = str.Substring(8);
                    }
                    var resultString = Encoding.UTF8.GetString(bytes);

                    //now lets parse answer to know if we are good
                    string[] msgSplit = resultString.Split(':');

                    // check if the logout succeeded
                    if (msgSplit[1].Contains("1"))
                    {
                        this.client.Close();
                        Environment.Exit(0);
                    }
                    else
                    {
                        string errorMsg = msgSplit[1].Replace("}", string.Empty);
                        errorMsg = errorMsg.Replace("\"", string.Empty);
                        MessageBox.Show(errorMsg, "Failed", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
            }
        }

        private void closeR(object sneder, RoutedEventArgs e)
        {
            closeRoom();
        }

        private void closeRoom()
        {
            flag = false;
            char msgCode = '1';
            string fullMsg = "{}";
            int msgLength = fullMsg.Length;

            var msgLengthBytes = msgLength.ToString("D4");

            fullMsg = msgCode.ToString() + msgLengthBytes + fullMsg;
            var b = string.Join("", Encoding.UTF8.GetBytes(fullMsg).Select(n => Convert.ToString(n, 2).PadLeft(8, '0')));
            Byte[] vs = new byte[b.Length];
            for (int i = 0; i < b.Length; i++)
            {
                vs[i] = Convert.ToByte(b[i]);
            }

            try
            {
                client.Send(vs);
                var answer = new byte[1026];
                int bytesRead = client.Receive(answer);

                string str = Encoding.Default.GetString(answer);
                str = str.Replace("\0", String.Empty);
                byte[] bytes = new byte[str.Length / 8];
                int j = 0;
                while (str.Length > 0)
                {
                    var result = Convert.ToByte(str.Substring(0, 8), 2);
                    bytes[j++] = result;
                    if (str.Length >= 8)
                        str = str.Substring(8);
                }
                var resultString = Encoding.UTF8.GetString(bytes);

                //now lets parse answer to know if we are good
                string[] msgSplit = resultString.Split('{');

                // get the rooms data
                string jsonString = "{" + msgSplit[1];

                if (jsonString.Contains("1"))
                {
                    MessageBox.Show("Closing the room...");

                    // redirect to the menu
                    var menu = new users_menu(username, client);
                    menu.Show();
                    isClose = false;
                    this.Close();
                }

            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void startGame(object sender, RoutedEventArgs e)
        {
            flag = false;
            char msgCode = '2';
            string fullMsg = "{}";
            int msgLength = fullMsg.Length;

            var msgLengthBytes = msgLength.ToString("D4");

            fullMsg = msgCode.ToString() + msgLengthBytes + fullMsg;
            var b = string.Join("", Encoding.UTF8.GetBytes(fullMsg).Select(n => Convert.ToString(n, 2).PadLeft(8, '0')));
            Byte[] vs = new byte[b.Length];
            for (int i = 0; i < b.Length; i++)
            {
                vs[i] = Convert.ToByte(b[i]);
            }

            try
            {
                client.Send(vs);
                var answer = new byte[1026];
                int bytesRead = client.Receive(answer);

                string str = Encoding.Default.GetString(answer);
                str = str.Replace("\0", String.Empty);
                byte[] bytes = new byte[str.Length / 8];
                int j = 0;
                while (str.Length > 0)
                {
                    var result = Convert.ToByte(str.Substring(0, 8), 2);
                    bytes[j++] = result;
                    if (str.Length >= 8)
                        str = str.Substring(8);
                }
                var resultString = Encoding.UTF8.GetString(bytes);

                //now lets parse answer to know if we are good
                string[] msgSplit = resultString.Split('{');

                // get the rooms data
                string jsonString = "{" + msgSplit[1];

                if (jsonString.Contains("1"))
                {
                    MessageBox.Show("The game start now!!!");

                    isClose = false;
                    // redirect to the game window
                }

            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }
        private void leaveRoom()
        {
            flag = false;
            char msgCode = '4';
            string fullMsg = "{}";
            int msgLength = fullMsg.Length;

            var msgLengthBytes = msgLength.ToString("D4");

            fullMsg = msgCode.ToString() + msgLengthBytes + fullMsg;
            var b = string.Join("", Encoding.UTF8.GetBytes(fullMsg).Select(n => Convert.ToString(n, 2).PadLeft(8, '0')));
            Byte[] vs = new byte[b.Length];
            for (int i = 0; i < b.Length; i++)
            {
                vs[i] = Convert.ToByte(b[i]);
            }

            try
            {
                client.Send(vs);
                var answer = new byte[1026];
                int bytesRead = client.Receive(answer);

                string str = Encoding.Default.GetString(answer);
                str = str.Replace("\0", String.Empty);
                byte[] bytes = new byte[str.Length / 8];
                int j = 0;
                while (str.Length > 0)
                {
                    var result = Convert.ToByte(str.Substring(0, 8), 2);
                    bytes[j++] = result;
                    if (str.Length >= 8)
                        str = str.Substring(8);
                }
                var resultString = Encoding.UTF8.GetString(bytes);

                //now lets parse answer to know if we are good
                string[] msgSplit = resultString.Split('{');

                // get the rooms data
                string jsonString = "{" + msgSplit[1];

                if (jsonString.Contains("1"))
                {
                    MessageBox.Show("Leaving the room...");

                    // redirect to the menu
                    var menu = new users_menu(username, client);
                    menu.Show();
                    isClose = false;
                    this.Close();
                }


            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }
    }
}
