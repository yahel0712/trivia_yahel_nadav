﻿using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows;

namespace GUI_WORKSHOP2
{
    /// <summary>
    /// Interaction logic for joinRoom.xaml
    /// </summary>
    public partial class joinRoom : Window
    {
        private Thread t;
        private volatile bool flag = true;
        private string username;
        private Socket client;
        private bool isClose = true;

        private void getRooms()
        {
            while (flag)
            {
                try
                {

                // wait 3 seconds till update again
                Thread.Sleep(TimeSpan.FromSeconds(6));
                this.Dispatcher.Invoke(new Action(() =>
                {
                    char msgCode = '6';
                    string fullMsg = "{}";
                    int msgLength = fullMsg.Length;

                    var msgLengthBytes = msgLength.ToString("D4");

                    fullMsg = msgCode.ToString() + msgLengthBytes + fullMsg;
                    var b = string.Join("", Encoding.UTF8.GetBytes(fullMsg).Select(n => Convert.ToString(n, 2).PadLeft(8, '0')));
                    Byte[] vs = new byte[b.Length];
                    for (int i = 0; i < b.Length; i++)
                    {
                        vs[i] = Convert.ToByte(b[i]);
                    }

                    try
                    {
                        ////sending the signup request
                        client.Send(vs);
                        //from now - enditing
                        var answer = new byte[1026];
                        int bytesRead = client.Receive(answer);

                        string str = Encoding.Default.GetString(answer);
                        str = str.Replace("\0", String.Empty);
                        byte[] bytes = new byte[str.Length / 8];
                        int j = 0;
                        while (str.Length > 0)
                        {
                            var result = Convert.ToByte(str.Substring(0, 8), 2);
                            bytes[j++] = result;
                            if (str.Length >= 8)
                                str = str.Substring(8);
                        }
                        var resultString = Encoding.UTF8.GetString(bytes);

                        //now lets parse answer to know if we are good
                        string[] msgSplit = resultString.Split('{');

                        // get the rooms data
                        string jsonString = "{" + msgSplit[1];

                        // parse the json string to json object
                        JObject o = JObject.Parse(jsonString);

                        string rooms = (string)o["Rooms"];
                        if (rooms != null)
                        {
                            rooms = rooms.Replace(",", "\n");
                        }

                        // show the rooms in the window
                        Options.Text = rooms;
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show(exc.Message);
                    }
                }));
                }
                catch (Exception)
                {

                }
            }
        }
        public joinRoom(string username, Socket client)
        {
            InitializeComponent();
            this.client = client;
            this.username = username;

            t = new Thread(() => getRooms());
            t.Start();
        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            flag = false;
            char msgCode = '4';
            string fullMsg = "{\"roomName\" : \"" + name.Text + "\",\"username\" : \"" + username + "\"}";
            int msgLength = fullMsg.Length;

            var msgLengthBytes = msgLength.ToString("D4");

            fullMsg = msgCode.ToString() + msgLengthBytes + fullMsg;
            var b = string.Join("", Encoding.UTF8.GetBytes(fullMsg).Select(n => Convert.ToString(n, 2).PadLeft(8, '0')));
            Byte[] vs = new byte[b.Length];
            for (int i = 0; i < b.Length; i++)
            {
                vs[i] = Convert.ToByte(b[i]);
            }

            try
            {
                ////sending the signup request
                client.Send(vs);
                //from now - enditing
                var answer = new byte[1026];
                int bytesRead = client.Receive(answer);

                string str = Encoding.Default.GetString(answer);
                str = str.Replace("\0", String.Empty);
                byte[] bytes = new byte[str.Length / 8];
                int j = 0;
                while (str.Length > 0)
                {
                    var result = Convert.ToByte(str.Substring(0, 8), 2);
                    bytes[j++] = result;
                    if (str.Length >= 8)
                        str = str.Substring(8);
                }
                var resultString = Encoding.UTF8.GetString(bytes);

                //now lets parse answer to know if we are good
                string[] msgSplit = resultString.Split(':');

                // check if the response is the right response
                var code = msgSplit[0].Split('{');
                while(code[0][0] != '4')
                {
                    ////sending the signup request
                    client.Send(vs);
                    //from now - enditing
                    answer = new byte[1026];
                    bytesRead = client.Receive(answer);

                    str = Encoding.Default.GetString(answer);
                    str = str.Replace("\0", String.Empty);
                    bytes = new byte[str.Length / 8];
                    j = 0;
                    while (str.Length > 0)
                    {
                        var result = Convert.ToByte(str.Substring(0, 8), 2);
                        bytes[j++] = result;
                        if (str.Length >= 8)
                            str = str.Substring(8);
                    }
                    resultString = Encoding.UTF8.GetString(bytes);

                    //now lets parse answer to know if we are good
                    msgSplit = resultString.Split(':');

                    // check if the response is the right response
                    code = msgSplit[0].Split('{');
                }
                if (msgSplit[1].Contains("1"))
                {
                    MessageBox.Show("User join to the room Successfully!", "Welcome", MessageBoxButton.OK);

                    t.Interrupt();
                    // redirect to the main menu                        
                    var MainMenuPage = new Room(client, this.name.Text, username);
                    isClose = false;
                    this.Close();
                    MainMenuPage.Show();
                }
                else
                {
                    string errorMsg = msgSplit[1].Replace("}", string.Empty);
                    errorMsg = errorMsg.Replace("\"", string.Empty);
                    MessageBox.Show(errorMsg, "Failed", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void btn_Copy_Click(object sender, RoutedEventArgs e)
        {
            // redirect back to the menu
            var menu = new users_menu(this.username, this.client);
            this.Close();
            menu.Show();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // check if the X button clicked
            if (isClose)
            {
                // send logout request to the server
                char msgCode = '3';
                string fullMsg = "{\"userName\" : \"" + username + "\"}";
                int msgLength = fullMsg.Length;

                var msgLengthBytes = msgLength.ToString("D4");

                fullMsg = msgCode.ToString() + msgLengthBytes + fullMsg;
                var b = string.Join("", Encoding.UTF8.GetBytes(fullMsg).Select(n => Convert.ToString(n, 2).PadLeft(8, '0')));
                Byte[] vs = new byte[b.Length];
                for (int i = 0; i < b.Length; i++)
                {
                    vs[i] = Convert.ToByte(b[i]);
                }

                try
                {
                    ////sending the signup request
                    client.Send(vs);
                    //from now - enditing
                    var answer = new byte[1026];
                    int bytesRead = client.Receive(answer);

                    string str = Encoding.Default.GetString(answer);
                    str = str.Replace("\0", String.Empty);
                    byte[] bytes = new byte[str.Length / 8];
                    int j = 0;
                    while (str.Length > 0)
                    {
                        var result = Convert.ToByte(str.Substring(0, 8), 2);
                        bytes[j++] = result;
                        if (str.Length >= 8)
                            str = str.Substring(8);
                    }
                    var resultString = Encoding.UTF8.GetString(bytes);

                    //now lets parse answer to know if we are good
                    string[] msgSplit = resultString.Split(':');

                    // check if the logout succeeded
                    if (msgSplit[1].Contains("1"))
                    {
                        this.client.Close();
                        Environment.Exit(0);
                    }
                    else
                    {
                        string errorMsg = msgSplit[1].Replace("}", string.Empty);
                        errorMsg = errorMsg.Replace("\"", string.Empty);
                        MessageBox.Show(errorMsg, "Failed", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
            }
        }
    }
}
