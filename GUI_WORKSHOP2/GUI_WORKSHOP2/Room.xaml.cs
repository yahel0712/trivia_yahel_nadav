﻿using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Threading;
using System.ComponentModel;

namespace GUI_WORKSHOP2
{
    /// <summary>
    /// Interaction logic for Room.xaml
    /// </summary>
    public partial class Room : Window
    {
        private Socket client;
        private string roomName;
        private string username;
        private volatile bool flag = true;
        private bool isClose = true;
        Thread roomState;

        private void getRoomState()
        {
            while (flag)
            {
                try
                {

                // wait 2 seconds till update again
                Thread.Sleep(TimeSpan.FromSeconds(5));
                this.Dispatcher.Invoke((() =>
                {
                    char msgCode = '3';
                    string fullMsg = "{}";
                    int msgLength = fullMsg.Length;

                    var msgLengthBytes = msgLength.ToString("D4");

                    fullMsg = msgCode.ToString() + msgLengthBytes + fullMsg;
                    var b = string.Join("", Encoding.UTF8.GetBytes(fullMsg).Select(n => Convert.ToString(n, 2).PadLeft(8, '0')));
                    Byte[] vs = new byte[b.Length];
                    for (int i = 0; i < b.Length; i++)
                    {
                        vs[i] = Convert.ToByte(b[i]);
                    }

                    try
                    {
                        client.Send(vs);
                        var answer = new byte[1026];
                        int bytesRead = client.Receive(answer);

                        string str = Encoding.Default.GetString(answer);
                        str = str.Replace("\0", String.Empty);
                        byte[] bytes = new byte[str.Length / 8];
                        int j = 0;
                        while (str.Length > 0)
                        {
                            var result = Convert.ToByte(str.Substring(0, 8), 2);
                            bytes[j++] = result;
                            if (str.Length >= 8)
                                str = str.Substring(8);
                        }
                        var resultString = Encoding.UTF8.GetString(bytes);

                        //now lets parse answer to know if we are good
                        string[] msgSplit = resultString.Split('{');

                        // get the rooms data
                        string jsonString = "{" + msgSplit[1];

                        // parse the json string to json object
                        JObject o = JObject.Parse(jsonString);

                        // check the status of the room
                        if ((string)o["status"] == "0")
                        {
                            flag = false;
                            leaveRoom();
                        }

                        room_Name.Text = this.roomName;
                        adminName.Text = (string)o["admin"];
                        users.Text = (string)o["players"];

                        // check if the game began
                        if ((string)o["hasGameBegan"] == "1")
                        {
                            MessageBox.Show("The game start now!!!");
                            roomState.Interrupt();
                            
                            // redirect to the game window
                        }
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show(exc.Message);
                    }
                }));
                }
                catch (Exception)
                {
                }
            }

        }
        

        public Room(Socket client, string roomName, string username)
        {
            InitializeComponent();
            this.client = client;
            this.roomName = roomName;
            this.username = username;

            // active the thread that get the room state
            roomState = new Thread(() => getRoomState());
            roomState.Start();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            flag = false;
            // check if the X button clicked
            if (isClose)
            {
                this.Dispatcher.Invoke((() =>
                {
                    roomState.Interrupt();
                }));
                // send leave room request to the server
                char msgCode = '4';
                string fullMsg = "{}";
                int msgLength = fullMsg.Length;

                var msgLengthBytes = msgLength.ToString("D4");

                fullMsg = msgCode.ToString() + msgLengthBytes + fullMsg;
                var b = string.Join("", Encoding.UTF8.GetBytes(fullMsg).Select(n => Convert.ToString(n, 2).PadLeft(8, '0')));
                Byte[] vs = new byte[b.Length];
                for (int i = 0; i < b.Length; i++)
                {
                    vs[i] = Convert.ToByte(b[i]);
                }

                try
                {
                    client.Send(vs);
                    var answer = new byte[1026];
                    int bytesRead = client.Receive(answer);

                    string str = Encoding.Default.GetString(answer);
                    str = str.Replace("\0", String.Empty);
                    byte[] bytes = new byte[str.Length / 8];
                    int j = 0;
                    while (str.Length > 0)
                    {
                        var result = Convert.ToByte(str.Substring(0, 8), 2);
                        bytes[j++] = result;
                        if (str.Length >= 8)
                            str = str.Substring(8);
                    }
                    var resultString = Encoding.UTF8.GetString(bytes);

                    //now lets parse answer to know if we are good
                    string[] msgSplit = resultString.Split('{');

                    // get the rooms data
                    string jsonString = "{" + msgSplit[1];

                    if (jsonString.Contains("1"))
                    {
                        MessageBox.Show("Leaving the room...");

                        isClose = false;
                        this.Close();
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }


                // send logout request to the server
                msgCode = '3';
                fullMsg = "{\"userName\" : \"" + username + "\"}";
                msgLength = fullMsg.Length;

                msgLengthBytes = msgLength.ToString("D4");

                fullMsg = msgCode.ToString() + msgLengthBytes + fullMsg;
                b = string.Join("", Encoding.UTF8.GetBytes(fullMsg).Select(n => Convert.ToString(n, 2).PadLeft(8, '0')));
                vs = new byte[b.Length];
                for (int i = 0; i < b.Length; i++)
                {
                    vs[i] = Convert.ToByte(b[i]);
                }

                try
                {
                    ////sending the signup request
                    client.Send(vs);
                    //from now - enditing
                    var answer = new byte[1026];
                    int bytesRead = client.Receive(answer);

                    string str = Encoding.Default.GetString(answer);
                    str = str.Replace("\0", String.Empty);
                    byte[] bytes = new byte[str.Length / 8];
                    int j = 0;
                    while (str.Length > 0)
                    {
                        var result = Convert.ToByte(str.Substring(0, 8), 2);
                        bytes[j++] = result;
                        if (str.Length >= 8)
                            str = str.Substring(8);
                    }
                    var resultString = Encoding.UTF8.GetString(bytes);

                    //now lets parse answer to know if we are good
                    string[] msgSplit = resultString.Split(':');

                    // check if the logout succeeded
                    if (msgSplit[1].Contains("1"))
                    {
                        this.client.Close();
                        Environment.Exit(0);
                    }
                    else
                    {
                        string errorMsg = msgSplit[1].Replace("}", string.Empty);
                        errorMsg = errorMsg.Replace("\"", string.Empty);
                        MessageBox.Show(errorMsg, "Failed", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
            }
        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            leaveRoom();
        }

        private void leaveRoom()
        {
            this.Dispatcher.Invoke((() =>
            {
                if (roomState.IsAlive)
                {
                    roomState.Interrupt();
                }
            }));
            flag = false;
            char msgCode = '4';
            string fullMsg = "{}";
            int msgLength = fullMsg.Length;

            var msgLengthBytes = msgLength.ToString("D4");

            fullMsg = msgCode.ToString() + msgLengthBytes + fullMsg;
            var b = string.Join("", Encoding.UTF8.GetBytes(fullMsg).Select(n => Convert.ToString(n, 2).PadLeft(8, '0')));
            Byte[] vs = new byte[b.Length];
            for (int i = 0; i < b.Length; i++)
            {
                vs[i] = Convert.ToByte(b[i]);
            }

            try
            {
                client.Send(vs);
                var answer = new byte[1026];
                int bytesRead = client.Receive(answer);

                string str = Encoding.Default.GetString(answer);
                str = str.Replace("\0", String.Empty);
                byte[] bytes = new byte[str.Length / 8];
                int j = 0;
                while (str.Length > 0)
                {
                    var result = Convert.ToByte(str.Substring(0, 8), 2);
                    bytes[j++] = result;
                    if (str.Length >= 8)
                        str = str.Substring(8);
                }
                var resultString = Encoding.UTF8.GetString(bytes);

                //now lets parse answer to know if we are good
                string[] msgSplit = resultString.Split('{');

                // get the rooms data
                string jsonString = "{" + msgSplit[1];

                /*// check if the response is the right response
                var code = msgSplit[0].Split('{');
                while(code[0][0] != '4')
                {
                    ////sending the signup request
                    client.Send(vs);
                    //from now - enditing
                    answer = new byte[1026];
                    bytesRead = client.Receive(answer);

                    str = Encoding.Default.GetString(answer);
                    str = str.Replace("\0", String.Empty);
                    bytes = new byte[str.Length / 8];
                    j = 0;
                    while (str.Length > 0)
                    {
                        var result = Convert.ToByte(str.Substring(0, 8), 2);
                        bytes[j++] = result;
                        if (str.Length >= 8)
                            str = str.Substring(8);
                    }
                    resultString = Encoding.UTF8.GetString(bytes);

                    //now lets parse answer to know if we are good
                    msgSplit = resultString.Split(':');

                    // check if the response is the right response
                    code = msgSplit[0].Split('{');
                }*/
                if (jsonString.Contains("1") || jsonString.Contains("\"status\":0"))
                {
                    MessageBox.Show("Leaving the room...");

                    // redirect to the menu
                    var menu = new users_menu(username, client);
                    isClose = false;
                    this.Close();
                    menu.Show();

                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
                
        }
    }
}
