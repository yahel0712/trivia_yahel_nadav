﻿using System;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Windows;

namespace GUI_WORKSHOP2
{
    /// <summary>
    /// Interaction logic for users_menu.xaml
    /// </summary>
    public partial class users_menu : Window
    {
        private string username;
        private Socket client;
        private bool exit;

        public users_menu(string userName, Socket client)
        {
            InitializeComponent();
            this.username = userName;
            this.client = client;
            this.exit = true;
        }

        private void createRoom(object sender, RoutedEventArgs e)
        {
            // redirect to create room window                       
            var createRoomWindow = new CreateRoom(this.client, this.username);
            exit = false;
            this.Close();
            createRoomWindow.Show();
        }

        private void logout(object sender, RoutedEventArgs e)
        {
            // send logout request to the server
            char msgCode = '3';
            string fullMsg = "{\"userName\" : \"" + username + "\"}";
            int msgLength = fullMsg.Length;

            var msgLengthBytes = msgLength.ToString("D4");

            fullMsg = msgCode.ToString() + msgLengthBytes + fullMsg;
            var b = string.Join("", Encoding.UTF8.GetBytes(fullMsg).Select(n => Convert.ToString(n, 2).PadLeft(8, '0')));
            Byte[] vs = new byte[b.Length];
            for (int i = 0; i < b.Length; i++)
            {
                vs[i] = Convert.ToByte(b[i]);
            }

            try
            {
                ////sending the signup request
                client.Send(vs);
                //from now - enditing
                var answer = new byte[1026];
                int bytesRead = client.Receive(answer);

                string str = Encoding.Default.GetString(answer);
                str = str.Replace("\0", String.Empty);
                byte[] bytes = new byte[str.Length / 8];
                int j = 0;
                while (str.Length > 0)
                {
                    var result = Convert.ToByte(str.Substring(0, 8), 2);
                    bytes[j++] = result;
                    if (str.Length >= 8)
                        str = str.Substring(8);
                }
                var resultString = Encoding.UTF8.GetString(bytes);

                //now lets parse answer to know if we are good
                string[] msgSplit = resultString.Split(':');

                // check if the logout succeeded
                if (msgSplit[1].Contains("1"))
                {
                    this.client.Close();
                    Environment.Exit(0);
                }
                else
                {
                    string errorMsg = msgSplit[1].Replace("}", string.Empty);
                    errorMsg = errorMsg.Replace("\"", string.Empty);
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void joinRoom(object sender, RoutedEventArgs e)
        {
            // redirect to the join rooms window
            var joinRoomWindow = new joinRoom(this.username, this.client);
            exit = false;
            this.Close();
            joinRoomWindow.Show();
        }

        private void getStats(object sender, RoutedEventArgs e)
        {
            // when window be ready
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // check if the X button clicked
            if (exit)
            {

                // send logout request to the server
                char msgCode = '3';
                string fullMsg = "{\"userName\" : \"" + username + "\"}";
                int msgLength = fullMsg.Length;

                var msgLengthBytes = msgLength.ToString("D4");

                fullMsg = msgCode.ToString() + msgLengthBytes + fullMsg;
                var b = string.Join("", Encoding.UTF8.GetBytes(fullMsg).Select(n => Convert.ToString(n, 2).PadLeft(8, '0')));
                Byte[] vs = new byte[b.Length];
                for (int i = 0; i < b.Length; i++)
                {
                    vs[i] = Convert.ToByte(b[i]);
                }

                try
                {
                    ////sending the signup request
                    client.Send(vs);
                    //from now - enditing
                    var answer = new byte[1026];
                    int bytesRead = client.Receive(answer);

                    string str = Encoding.Default.GetString(answer);
                    str = str.Replace("\0", String.Empty);
                    byte[] bytes = new byte[str.Length / 8];
                    int j = 0;
                    while (str.Length > 0)
                    {
                        var result = Convert.ToByte(str.Substring(0, 8), 2);
                        bytes[j++] = result;
                        if (str.Length >= 8)
                            str = str.Substring(8);
                    }
                    var resultString = Encoding.UTF8.GetString(bytes);

                    //now lets parse answer to know if we are good
                    string[] msgSplit = resultString.Split(':');

                    // check if the logout succeeded
                    if (msgSplit[1].Contains("1"))
                    {
                        this.client.Close();
                        Environment.Exit(0);
                    }
                    else
                    {
                        string errorMsg = msgSplit[1].Replace("}", string.Empty);
                        errorMsg = errorMsg.Replace("\"", string.Empty);
                        MessageBox.Show(errorMsg, "Failed", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
            }
        }
    }
}
