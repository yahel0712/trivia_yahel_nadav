﻿namespace GUI_WORKSHOP2
{
    class login_msg
    {
        public login_msg(string username, string password)
        {
            this._username = username;
            this._password = password;
        }
        public string _username;
        public string _password;
    }

    class signup_msg
    {
        public signup_msg(string username, string password, string mail,
            string address, string phone, string birthday)
        {
            this._username = username;
            this._password = password;
            this._email = mail;
            this._phone = phone;
            this._address = address;
            this._birthday = birthday;
        }
        public string _username;
        public string _password;
        public string _email;
        public string _address;
        public string _phone;
        public string _birthday;
    }
}
